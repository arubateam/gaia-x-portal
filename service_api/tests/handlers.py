import server
import json
from tornado.testing import AsyncHTTPTestCase
import logging
import tornado.testing

logger = logging.getLogger("tornado.application")


class BaseTest(AsyncHTTPTestCase):
    def get_app(self):
        self._app = server.make_app()
        return self._app

    def reverse_url(self, name, *args):
        return self.get_url(self._app.reverse_url(name, *args))


class InfoTest(BaseTest):
    @tornado.testing.gen_test
    async def test_cors(self):
        response = await self.http_client.fetch(self.reverse_url('info'), method='OPTIONS')
        self.assertEqual(response.code, 204)
        headers = tornado.httputil.HTTPHeaders(response.headers)
        self.assertIn('Access-Control-Allow-Origin', headers)
        self.assertEqual(headers['Access-Control-Allow-Origin'], '*')

    @tornado.testing.gen_test
    async def test_version_available(self):
        response = await self.http_client.fetch(self.reverse_url('info'))
        self.assertEqual(response.code, 200)
        headers = tornado.httputil.HTTPHeaders(response.headers)
        self.assertIn('content-type', headers)
        self.assertEqual(headers['content-type'], 'application/json')
        self.assertIn('version', json.loads(response.body))

    @tornado.testing.gen_test
    async def test_spec(self):
        response = await self.http_client.fetch(self.reverse_url('spec'))
        self.assertEqual(response.code, 200)
        headers = tornado.httputil.HTTPHeaders(response.headers)
        self.assertIn('content-type', headers)
        self.assertEqual(headers['content-type'], 'text/x-yaml')
        # logger.debug(response.body.decode())
