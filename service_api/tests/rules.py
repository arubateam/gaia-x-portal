import unittest
from server.rules import Attribute, Relation, And, Or

class RelationTest(unittest.TestCase):
    def test_rule_eq(self):
        self.assertEqual(Relation("a", "=", "a"), Relation("a", "=", "a"))
        self.assertEqual(Relation("abc", "with", "1", neg=True), Relation("abc", "with", "1", neg=True))

    def test_rule_neq(self):
        self.assertNotEqual(Relation("a", "=", "a"), Relation("b", "=", "a"))
        self.assertNotEqual(Relation("a", "=", "a"), Relation("a", "!=", "a"))
        self.assertNotEqual(Relation("a", "=", "a"), Relation("a", "=", "a", neg=True))

    def test_logic_eq(self):
        self.assertEqual(Or(Relation("a", "=", "a"), Relation("b", "=", "a")), Or(Relation("b", "=", "a"), Relation("a", "=", "a")))
        self.assertEqual(And(Relation("a", "=", "a"), Relation("b", "=", "a")), And(Relation("b", "=", "a"), Relation("a", "=", "a")))

    def test_logic_neq(self):
        self.assertNotEqual(Or(Relation("a", "=", "a"), Relation("b", "=", "a")), And(Relation("b", "=", "a"), Relation("a", "=", "a")))
        self.assertNotEqual(Or(Relation("a", "=", "a"), Relation("b", "=", "a")), Or(Relation("a", "=", "a")))


class AttributeTest(unittest.TestCase):
    def test_attr_eq(self):
        self.assertEqual(Attribute("a", "1", "=", "a"), Attribute("a", "1", "=", "a"))

    def test_attr_neq(self):
        self.assertNotEqual(Attribute("a", "2", "=", "a"), Attribute("a", "1", "=", "a"))
