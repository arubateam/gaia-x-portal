import server
import unittest
from . import handlers
from . import handlers_db
from . import handlers_ui
from . import query
from . import rules
import sys

suites = []

if len(sys.argv) > 1:
    suites.append(unittest.TestLoader().loadTestsFromModule(sys.modules["tests.{}".format(sys.argv[1])]))
else:
    suites.append(unittest.TestLoader().loadTestsFromModule(rules))
    suites.append(unittest.TestLoader().loadTestsFromModule(handlers))
    suites.append(unittest.TestLoader().loadTestsFromModule(handlers_db))
    suites.append(unittest.TestLoader().loadTestsFromModule(handlers_ui))
    suites.append(unittest.TestLoader().loadTestsFromModule(query))
suite = unittest.TestSuite(suites)
results = unittest.TextTestRunner(verbosity=2).run(suite)
