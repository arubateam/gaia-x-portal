MATCH (n) DETACH DELETE n;
MATCH (n) RETURN count(n) AS count;

////---------------

CREATE (a:Letter:Symbol {name:'A', type: 'letter'})
CREATE (b:Letter:Symbol {name:'B', type: 'letter'})
CREATE (c:Letter:Symbol {name:'C', type: 'letter'})
CREATE (alphabet:Group {name:'Alphabet', type: 'group'})
CREATE (int1:Integer:Symbol {name:'1', type: 'number'})
CREATE (int2:Integer:Symbol {name:'2', type: 'number'})
CREATE (R:Group {name:'R', type: 'group'})
CREATE (all:All {name:'all', type: 'all'})

CREATE (a)-[:IN]->(alphabet),
       (b)-[:IN]->(alphabet),
       (c)-[:IN]->(alphabet),
       (int1)-[:IN {}]->(R),
       (int2)-[:IN {}]->(R),
       (alphabet)-[:IS]->(all),
       (R)-[:IS]->(all);

////---------------

CREATE (storage:Service {name:'storage'})
CREATE (provider:Provider {name:'provider'})

CREATE (storage)-[:HOSTED_BY]->(provider);

////---------------

MATCH (n) RETURN count(n) AS count;
