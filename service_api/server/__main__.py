#!/usr/bin/env python3

import tornado.httpserver
import tornado.ioloop
import tornado.options
from . import make_app, options

application = make_app()
http_server = tornado.httpserver.HTTPServer(application)
http_server.listen(options.port)
tornado.ioloop.IOLoop.current().start()
