from tornado.web import HTTPError
import http
from tornado.httpclient import AsyncHTTPClient
from typing import Union
from .handlers import return_json
from .handlers_db import DbBaseHandler, NodeLabelsHandler, NodeLabelNamesHandler, SearchHandler, FindHandler, \
    FetchHandler
from marshmallow import Schema, fields
import json

################################################ SELECTOR ################################################
from .rules import Node


class SelectorSchema(Schema):
    field = fields.String()
    options = fields.List(fields.String())
    edge = fields.String(required=False)
    property = fields.String(required=False)


class SelectorsSchema(Schema):
    selectors = fields.List(fields.Nested(SelectorSchema))


async def get_selector(handler, label_limit=None):
    """Fetch selectors from the database.

    :param handler: handler to use for accessing the database.
    :param label_limit: if set to a label name, will fetch only the information regarding
        this label.
    :return: the selectors, following `class:SelectorSchema`
    """
    selectors = []
    labels = await NodeLabelsHandler.get_labels(handler.db, label_limit=label_limit)
    for label in labels:
        names = await NodeLabelNamesHandler.get_names(handler.db, label)
        selector = {'field': label, 'options': names}
        query = "MATCH ()-[r]->(b:{0!s}) return DISTINCT TYPE(r) as rel".format(label)
        res = (await handler.db.run(query)).single()
        if res:
            selector['edge'] = res['rel']
        else:
            selector['property'] = 'name'
        selectors.append(selector)
    return SelectorsSchema().dump({'selectors': selectors})


class SelectorHandler(DbBaseHandler):
    @return_json
    async def get(self):
        """---
description: '[UI] List of Selector'
responses:
  200:
    description: return list of UI selector
    content: 
      application/json:
        schema: SelectorsSchema
"""
        return await get_selector(self)


class SelectorLabelHandler(DbBaseHandler):
    @return_json
    async def get(self, label):
        """---
description: '[UI] Get specific selector'
parameters:
  - in: path
    name: label
    schema:
      type: string
    description: Label of the selector to get. If not given, all selectors are returned
responses:
  200:
    description: return a specific UI selector
    content:
      application/json:
        schema: SelectorsSchema
"""
        return await get_selector(self, label_limit=label)


################################################ SEARCH ################################################

class UINodeSchema(Schema):
    id = fields.Integer(required=True)
    labels = fields.List(fields.String(), required=True)
    properties = fields.Mapping(keys=fields.String())


class UISearchResultSchema(Schema):
    query = fields.String()
    nodes = fields.List(fields.Nested(UINodeSchema))


class UISearchHandler(DbBaseHandler):
    async def search(self, query):
        query, nodes = await SearchHandler.search(self.db, query)
        for idx in range(len(nodes)):
            if 'Service' in nodes[idx]['labels']:
                print(nodes[idx]['id'])
                _, providers = await FindHandler.find(self.db, 'Provider', nodes[idx]['id'])
                if len(providers):
                    nodes[idx]['properties']['provider'] = providers[0]
        return UISearchResultSchema().dump({'query': query, 'nodes': nodes})

    @return_json
    async def post(self):
        """---
description: '[UI] Database query'
parameters:
  - in: body
    name: query
    schema:
      type: object
    description: object as returned by the PEG parser
responses:
  200:
    description: return list of database node
    content:
      application/json:
        schema: UISearchResultSchema
"""
        if self.request.headers['Content-Type'] != 'application/json':
            raise HTTPError(http.client.BAD_REQUEST, "unknown content-type. Expecting 'application/json'")
        try:
            args = json.loads(self.request.body)
            query = args.get('query', None)
            if not query:
              raise HTTPError(http.client.BAD_REQUEST, "missing query parameter 'query'")
            query = json.dumps(query) # convert it back to string because it'll be deserialized later with custom class
            return await self.search(query)
        except TypeError as ex:
            raise HTTPError(http.client.BAD_REQUEST, "wrong query format: {}".format(ex))

    @return_json
    async def get(self):
        """---
description: '[UI] Database query'
parameters:
  - in: query
    name: q
    schema:
      type: string
    description: object as returned by the PEG parser
responses:
  200:
    description: return list of database node
    content:
      application/json:
        schema: UISearchResultSchema
"""
        query = self.get_argument("q", None)
        if not query:
            raise HTTPError(http.client.BAD_REQUEST, "missing query parameter 'q'")
        return await self.search(query)


class FetchQuerySchema(Schema):
    property = fields.String()
    value = fields.Raw()


class UIFetchResultSchema(Schema):
    query = fields.Nested(FetchQuerySchema)
    tree = fields.Dict()


class UIFetchHandler(DbBaseHandler):

    async def search(self, id):
        # fetch_query = FetchQuerySchema().loads(json_data=query)
        _, tree = await FetchHandler.search(self.db, id)
        return UIFetchResultSchema().dump({'id': id, 'tree': tree})

    @return_json
    async def post(self):
        """---
description: '[UI] fetch a complete self-description from database.'
parameters:
  - in: body
    name: query
    content:
      application/json:
        schema: FetchQuerySchema
    description: property and its value to use to filter for the self-description.
responses:
  200:
    description: return the nodes that belong to the self-description as tree.
    content:
      application/json:
        schema: UIFetchResultSchema
"""
        if self.request.headers['Content-Type'] != 'application/json':
            raise HTTPError(http.client.BAD_REQUEST, "unknown content-type. Expecting 'application/json'")
        try:
            args = json.loads(self.request.body)
            query = args.get('query', None)
            if not query:
                raise HTTPError(http.client.BAD_REQUEST, "missing query parameter 'query'")
            query = json.dumps(
                query)  # convert it back to string because it'll be deserialized later with custom class
            return await self.search(query)
        except TypeError as ex:
            raise HTTPError(http.client.BAD_REQUEST, "wrong query format: {}".format(ex))

    @return_json
    async def get(self):
        """---
description: '[UI] fetch a complete self-description from database.'
parameters:
  - in: query
    name: q
    content:
      application/json:
        schema: FetchQuerySchema
    description: property and its value to use to filter for the self-description.
responses:
  200:
    description: return the nodes that belong to the self-description as tree.
    content:
      application/json:
        schema: UIFetchResultSchema
"""
        id = self.get_argument("id", None)
        if not id:
            raise HTTPError(http.client.BAD_REQUEST, "missing query parameter 'id'")
        return await self.search(id)


################################################ LIST ################################################


class ListResultSchema(Schema):
    nodes = fields.List(fields.Nested(UINodeSchema))


class ListHandler(DbBaseHandler):

    @return_json
    async def get(self, label):
        """---
description: List of all Nodes with the provided label.
parameters:
  - in: path
    name: label
    schema:
      type: string
    description: node label to search for
responses:
  200:
    description: return list of nodes.
    content:
      application/json:
        schema: NodeLabelsSchema
"""
        query = f"MATCH (n:{label}) RETURN n"
        nodes = []
        for row in await self.db.run(query):
            row = row[0]

            # Format the Neo4J date structure
            properties = {k: str(v) for k, v in row.items()}
            if 'Service' in row.labels:
              print(row.id)
              _, providers = await FindHandler.find(self.db, 'Provider', row.id)
              if len(providers):
                properties['provider'] = providers[0]

            node = Node(
                id=row.id,
                labels=list(row.labels),
                properties=properties
            )
            nodes.append(node)
        return ListResultSchema().dump({"nodes": nodes})
