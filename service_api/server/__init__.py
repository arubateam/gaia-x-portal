import os
from distutils.util import strtobool
from tornado.options import define, options, parse_command_line
from tornado.web import URLSpec, RedirectHandler, Application
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.tornado import TornadoPlugin
from . import handlers
from . import handlers_db
from . import handlers_ui
import logging


logger = logging.getLogger("tornado.application")
logger.setLevel(logging.DEBUG)

define("port", default=8888, help="run on the given port", type=int)
define('debug', metavar='DEBUG', default=strtobool(os.environ.get('DEBUG', "false")), help='debug',  type=bool)
define("neo4j_user", metavar='NEO4J_USERNAME', default=os.environ.get('NEO4J_USERNAME', 'neo4j'), help="NEO4J username", type=str)
define("neo4j_pass", metavar='NEO4J_PASSWORD', default=os.environ.get('NEO4J_PASSWORD', 'admin'), help="NEO4J password", type=str)
define("neo4j_address", metavar='NEO4J_ADDRESS', default=os.environ.get('NEO4J_ADDRESS', 'bolt://localhost:7687'), help="NEO4J address", type=str)

spec = APISpec(
    title="Gaia-x demonstrator",
    version="1.0.0",
    info={
        'description': 'demonstrator API documentation',
        'license': {
            'name': 'BSD-3 Clause',
            'url': 'https://opensource.org/licenses/BSD-3-Clause'
        }
    },
    servers=[{
        'url': os.environ.get('SERVER_URL', 'https://staging.gaia-x-demonstrator.eu/api/v1/')
    }],
    openapi_version="3.0.3",
    plugins=[TornadoPlugin(), MarshmallowPlugin()],
)

def make_app():
    parse_command_line()
    routes = [
        URLSpec(r'/ui/selector/(?P<label>\w+)', handlers_ui.SelectorLabelHandler, name='ui.selector.label'),
        URLSpec(r'/ui/selector', handlers_ui.SelectorHandler, name='ui.selector'),
        URLSpec(r'/ui/search', handlers_ui.UISearchHandler, name='ui.search'),
        URLSpec(r'/ui/fetch', handlers_ui.UIFetchHandler, name='ui.fetch'),
        URLSpec(r'/ui/all/(?P<label>\w+)', handlers_ui.ListHandler, name='db.graphviz'),
        URLSpec(r'/db/node/(?P<label>\w+)', handlers_db.NodeLabelNamesHandler, name='db.node.label'),
        URLSpec(r'/db/node', handlers_db.NodeLabelsHandler, name='db.node'),
        URLSpec(r'/db/edge/(?P<link_type>\w+)', handlers_db.EdgeTypeLabelsHandler, name='db.edge.type'),
        URLSpec(r'/db/edge', handlers_db.EdgeTypesHandler, name='db.edge'),
        URLSpec(r'/db/find/(?P<label>\w+)', handlers_db.FindHandler, name='db.find'),
        URLSpec(r'/db/search', handlers_db.SearchHandler, name='db.search'),
        URLSpec(r'/db/grammar', handlers_db.GrammarHandler, name='db.grammar'),
        URLSpec(r'/db/graph', handlers_db.GraphvizHandler, name='db.graphviz'),
        URLSpec(r'/info/spec', handlers.SpecHandler, name='spec'),
        URLSpec(r'/info', handlers.InfoHandler, name='info'),
    ]
    for route in routes:
        spec.path(urlspec=route)
    routes += [
        URLSpec(r'/', RedirectHandler, {"url": r'info/spec'})
    ]


    settings = {
        'options': options,
        'version': os.environ.get('VERSION', 'dev'), # to be set by Dockerfile via CI_COMMIT_SHA
        'template_path': 'templates',
        'spec': spec
    }
    application = Application(routes, debug=options.debug, **settings)
    return application
