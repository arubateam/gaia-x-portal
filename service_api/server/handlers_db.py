from tornado.web import RequestHandler, HTTPError
import http
from tornado import template
import logging
import server.db
from .handlers import BaseHandler, return_json
import neobolt.exceptions
import asyncio
from .rules import expression_decoder, check_node_label, check_relation_type, check_node_id, Node
from json import JSONDecodeError
import json
from collections import namedtuple
from marshmallow import Schema, fields


logger = logging.getLogger("tornado.application")


class DbBaseHandler(BaseHandler):
  def prepare(self):
    super().prepare()
    try:
      self.db = server.db.GraphDB(address=self.application.settings['options'].neo4j_address,
                username=self.application.settings['options'].neo4j_user,
                password=self.application.settings['options'].neo4j_pass)
    except neobolt.exceptions.ServiceUnavailable as e:
      raise HTTPError(http.client.INTERNAL_SERVER_ERROR, "GraphDB error: {}".format(e))

################################################ GRAPHVIZ ################################################

class GraphvizHandler(DbBaseHandler):
    FORMATS = {
        'raw': 'text/plain',
        'dot': 'text/plain',
        'jpg': 'image/jpeg',
        'png': 'image/png',
        'svg': 'image/svg+xml',
        'pdf': 'application/pdf',
        'json': 'application/json'
    }
    async def get(self):
        """---
description: Database ontology visualization
parameters:
  - in: query
    name: lang
    schema:
      type: string
      enum: [dot, jpg, json, pdf, png, raw, svg]
    description: default=svg
responses:
  200:
    description: return database ontology as graph
    content:
      text/plain:
        schema:
          type: string
      image/jpeg:
        schema:
          type: object
          format: binary
      image/png:
        schema:
          type: object
          format: binary
      image/svg+xml:
        schema:
          type: string
      application/pdf:
        schema:
          type: object
          format: binary
      application/json:
        schema:
          type: string
"""
        lang = self.get_argument('lang', 'svg')
        if lang not in self.FORMATS.keys():
            raise HTTPError(http.client.BAD_REQUEST, "unknown format {}. valid ones are {}".format(lang, ", ".join(self.FORMATS.keys())))
        self.set_header('Content-Type', self.FORMATS[lang])
        query = "MATCH (a) UNWIND LABELS(a) AS label RETURN DISTINCT label"
        nodes = list(map(lambda r: r['label'], await self.db.run(query)))

        rules = []
        query = """
MATCH (a)-[r]->(b)
UNWIND LABELS(a) AS src
UNWIND LABELS(b) as dst
RETURN DISTINCT src, TYPE(r)AS rel, dst
"""
        for row in await self.db.run(query):
            rule = (row['src'], row['rel'], row['dst'])
            rules.append(rule)

        groups = []
        query = "MATCH (a)-[]->() RETURN DISTINCT LABELS(a) AS labels"
        for row in await self.db.run(query):
            for i in range(1, len(row['labels'])):
                groups.append((row['labels'][i-1], row['labels'][i]))

        content = self.render_string("graphviz.dot.j2", nodes=nodes, rules=rules, groups=groups)
        if lang == 'raw':
            self.write(content)
            return
        proc = await asyncio.create_subprocess_exec("dot", "-T{}".format(lang),
            stdin=asyncio.subprocess.PIPE, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
        stdout, stderr = await proc.communicate(input=content)
        self.write(stdout)

################################################ GRAMMAR ################################################

class GrammarHandler(DbBaseHandler):
    async def get(self):
        """---
description: Search grammar
responses:
  200:
    description: return grammar for https://pegjs.org/
    content:
      text/plain:
        schema:
          type: string
"""
        rules = []
        dests = {}
        srcs = {}
        query = """
MATCH p=((a)-[*]->(b))
WHERE length(p) < 10
    AND LABELS(a) <> LABELS(b)
UNWIND LABELS(a) AS src
UNWIND LABELS(b) as dst
RETURN DISTINCT src, TYPE(LAST(RELATIONSHIPS(p))) AS rel, dst
"""
        for row in await self.db.run(query):
            rule = (row['src'], row['rel'], row['dst'])
            rules.append(rule)
            dests[row['dst']] = []
            srcs[row['src']] = []

        for node in dests:
            dests[node] = list(map(lambda r: r['values'], await self.db.run('MATCH (a:{0!s}) RETURN DISTINCT a.name AS values'.format(node))))

        for node in srcs:
            srcs[node] = list(map(lambda r: r['values'], await self.db.run('match(n:{0!s}) UNWIND KEYS(n) AS values RETURN DISTINCT values'.format(node))))

        self.set_header('Content-Type', 'text/javascript')
        content = self.render_string("peg.js.j2", rules=rules, dests=dests, srcs=srcs, debug=self.application.settings['options'].debug)
        # logger.debug(content.decode())
        self.write(content)

################################################ SEARCH ################################################

class NodeSchema(Schema):
    id = fields.Integer(required=True)
    labels = fields.List(fields.String(), required=True)
    properties = fields.Mapping(keys=fields.String())


class SearchResultSchema(Schema):
    query = fields.String()
    nodes = fields.List(fields.Nested(NodeSchema))


class SearchHandler(DbBaseHandler):
    @staticmethod
    async def search(db, query):
        try:
            query = expression_decoder(query)
        except JSONDecodeError as ex:
            raise HTTPError(http.client.BAD_REQUEST, "wrong query format: {}".format(ex))
        logger.info("QUERY: {}".format(query))
        try:
            nodes = await query.run(db)
        except AssertionError as e:
            raise HTTPError(http.client.BAD_REQUEST, '{}: {}'.format(e, query))
        return query, nodes


    @return_json
    async def get(self):
        """---
description: Database query
parameters:
  - in: query
    name: q
    schema:
      type: string
    description: object as returned by the PEG parser
responses:
  200:
    description: return list of database node
    content: 
      application/json:
        schema: SearchResultSchema
"""
        query = self.get_argument("q", None)
        if not query:
            raise HTTPError(http.client.BAD_REQUEST, "missing query parameter 'q'")
        query, nodes = await self.search(self.db, query)
        return SearchResultSchema().dump({'query': query, 'nodes': nodes})


class FetchHandler(DbBaseHandler):

    @staticmethod
    async def search(db, id: int) -> (int, list):
        # Fetch all nodes bound to a node filtered by the provided parameter. This
        # assumes that the self-description files are imported as graphs of nodes
        # independent from each other.
        # Any parameter provided is fine as long as it only refers at least one node of
        # the graph.
        base_query = (
            "MATCH p=(n)-[*]->(m) WHERE id(n)={} WITH COLLECT(p) AS ps CALL apoc.convert.toTree(ps) yield value RETURN value"
        )
        db_query = base_query.format(id)
        logger.info("QUERY: {}".format(db_query))

        try:
            data = []
            for row in await db.run(db_query):
                for record in row:
                    data.append(record)
        except AssertionError as e:
            raise HTTPError(http.client.BAD_REQUEST, '{}: {}'.format(e, id))
        return id, data[0]


################################################ NODE ################################################

class NodeLabelsSchema(Schema):
    labels = fields.List(fields.String())


class NodeLabelsHandler(DbBaseHandler):
    @staticmethod
    async def get_labels(db, label_limit=None):
        if label_limit:
            match_query = f"(b:{label_limit})-[*]->(a)"
        else:
            match_query = "(a)"

        query = f"MATCH {match_query} UNWIND LABELS(a) as label RETURN DISTINCT label"
        labels = list(map(lambda r: r['label'], await db.run(query)))
        return labels

    @return_json
    async def get(self):
        """---
description: List of Node
responses:
  200:
    description: return list of node labels
    content: 
      application/json:
        schema: NodeLabelsSchema
"""
        labels = await self.get_labels(self.db)
        return NodeLabelsSchema().dump({'labels': labels})


class NodeLabelNamesSchema(Schema):
    label = fields.String()
    names = fields.List(fields.String())


class NodeLabelNamesHandler(DbBaseHandler):
    @staticmethod
    async def get_names(db, label):
        try:
            await check_node_label(db, label)
        except AssertionError as ex:
            raise HTTPError(http.client.BAD_REQUEST, "invalid label '{}'".format(label))
        query = "match (n:{0!s}) RETURN DISTINCT n.name AS name".format(label)
        names = list(map(lambda r: r['name'], await db.run(query)))
        return names

    @return_json
    async def get(self, label):
        """---
description: List of Node names
parameters:
  - in: path
    name: label
    schema:
      type: string
    description: node label
responses:
  200:
    description: return list of node label names
    content: 
      application/json:
        schema: NodeLabelNamesSchema
"""
        names = await self.get_names(self.db, label)
        return NodeLabelNamesSchema().dump({'label': label, 'names': names})

################################################ EDGE ################################################

class EdgeTypesSchema(Schema):
    types = fields.List(fields.String())


class EdgeTypesHandler(DbBaseHandler):
    @return_json
    async def get(self):
        """---
description: List of Edge types
responses:
  200:
    description: return list of edge types
    content: 
      application/json:
        schema: EdgeTypesSchema
"""
        query = "MATCH ()-[r]->() RETURN DISTINCT TYPE(r) AS type"
        types = list(map(lambda r: r['type'], await self.db.run(query)))
        return EdgeTypesSchema().dump({'types': types})


class EdgeTypeLabelsSchema(Schema):
    type = fields.String()
    labels = fields.List(fields.String())


class EdgeTypeLabelsHandler(DbBaseHandler):
    @return_json
    async def get(self, link_type):
        """---
description: List of Edge types names
parameters:
  - in: path
    name: link_type
    schema:
      type: string
    description: edge link type
responses:
  200:
    description: return list of edge type names
    content: 
      application/json:
        schema: EdgeTypeLabelsSchema
"""
        try:
            await check_relation_type(self.db, link_type)
        except AssertionError as ex:
            raise HTTPError(http.client.BAD_REQUEST, "invalid type '{}'".format(link_type))
        query = "MATCH ()-[r:{0!s}]->(b) UNWIND LABELS(b) AS label RETURN DISTINCT label".format(link_type)
        labels = list(map(lambda r: r['label'], await self.db.run(query)))
        return EdgeTypeLabelsSchema().dump({'type': link_type, 'labels': labels})

################################################ FIND ################################################

class FindHandler(DbBaseHandler):
    @staticmethod
    async def find(db, label, id):
        try:
            await check_node_label(db, label)
        except AssertionError as ex:
            raise HTTPError(http.client.BAD_REQUEST, "invalid label '{}'".format(label))
        if not isinstance(id, int):
          try:
              id = int(id, base=10)
          except ValueError as ex:
              raise HTTPError(http.client.BAD_REQUEST, "wrong id format: {}".format(ex))
        try:
            await check_node_id(db, id)
        except AssertionError as ex:
            raise HTTPError(http.client.BAD_REQUEST, "invalid id '{}'".format(id))

        query = "MATCH (a)-[*]->(node:{!s}) WHERE ID(a) = {:d} RETURN node".format(label, id)
        nodes = []
        for row in await db.run(query):
            nodes.append(Node(id=row['node'].id, labels=list(row['node'].labels), properties=dict(zip(row['node'].keys(), row['node'].values()))))
        return query, nodes


    @return_json
    async def get(self, label):
        """---
description: Find related node
parameters:
  - in: path
    name: label
    schema:
      type: string
    description: node label to search for
  - in: query
    name: id
    required: true
    schema:
      type: integer
    description: node id to search from
responses:
  200:
    description: return list of node
    content:
      application/json:
        schema: SearchResultSchema
"""
        id = self.get_argument("id", None)
        if not id:
            raise HTTPError(http.client.BAD_REQUEST, "missing id parameter 'id'")
        query, nodes = await self.find(self.db, label, id)
        return SearchResultSchema().dump({'query': query, 'nodes': nodes})
