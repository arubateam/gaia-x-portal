# Portal

Main url is https://portal.gaia-x-demonstrator.eu/

# Gitlab setup

## Kubernetes integration

Install:

- Ingress
- Prometheus
- Gitlab Runner
- Elastic Stack

## Private runner

Using a dedicated runner on the cluster is faster and have less network issue.

Make sure to set `DOCKER_HOST: tcp://localhost:2375` and `DOCKER_TLS_CERTDIR: ""` as CI envrironment variables because we are using our own k8s runner without TLS.

## Cert-manager

The `cert-manager` integration with Gitlab didn't setup the proper certificats. We installed and configured `cert-manager` manually with `helm`

### Installation with Helm 3

```shell
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v1.0.3 --set installCRDs=true
```

### Configuration

#### Step 1: Create cluster issuers

```
kubectl apply -f cert-manager-clusterissuer.yaml
```

#### Step 2: Add ingress annotation

```
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  annotations:
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
```

# CI/CD Issues

## could not get information about the resource: roles.rbac.authorization.k8s.io

This error can occur if the `service account` has been bound with the `edit` role.

```
$ helm install release http://release_url.tar.gz -f values.yaml
Error: rendered manifests contain a resource that already exists. Unable to continue with install: could not get information about the resource: roles.rbac.authorization.k8s.io "some_role" is forbidden: User "system:serviceaccount:demo-XXX:demo-XXX-service-account" cannot get resource "roles" in API group "rbac.authorization.k8s.io" in the namespace "demo-XXX"
```

### Solution

Find the proper `RoleBinding` to delete with those commands:

```
kubectl -n demo-XXX get rolebindings -o wide
kubectl -n demo-XXX get rolebindings gitlab-demo-XXX -o yaml
```

Once the `RoleBinding` has been found, delete it

```
kubectl -n demo-portal delete rolebindings gitlab-demo-XXX
```

And recreate it with `cluster-admin` role.

```
cat <<EOF | kubectl apply -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: gitlab-demo-XXX
  namespace: demo-XXX
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: demo-XXX-service-account
  namespace: demo-XXX
EOF
```
