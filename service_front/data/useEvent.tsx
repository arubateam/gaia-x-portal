import React, { useEffect } from "react";
import { EventEmitter } from "events";

const eventEmitter = new EventEmitter();

export const useEvent = (eventId, handler, deps = []) => {
  useEffect(() => {
    eventEmitter.on(eventId, handler);
    return () => eventEmitter.off(eventId, handler);
  }, deps);
};

export const trigger = (eventName, payload) => {
  return eventEmitter.emit(eventName, payload);
};
