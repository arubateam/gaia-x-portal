import React from "react";
import { makeStyles, AppBar, Toolbar, Button, Link } from "./index";

const logo = "/images/gaia-x-logo.svg";

const useStyles = makeStyles(
  (theme) => ({
    root: {
      zIndex: theme.zIndex.drawer + 1,
      left: theme.spacing(2),
      right: theme.spacing(2),
      bottom: theme.spacing(2),
      width: `calc(100% - ${theme.spacing(4)}px)`,
      position: "fixed",
      boxShadow: theme.dimensions.shadowPopover,
      borderRadius: theme.shape.borderRadius,
      [theme.breakpoints.up("md")]: {
        height: `calc(100vh - ${theme.spacing(4)}px)`,
        top: theme.spacing(2),
        width: theme.dimensions.navigationWidth,
        padding: theme.spacing(2, 0),
      },
    },
    toolbar: {
      flexDirection: "row",
      alignItems: "inherit",
      minHeight: "100%",
      margin: 0,
      listStyle: "none",
      justifyContent: "space-evenly",
      padding: 0,
      [theme.breakpoints.up("md")]: {
        flexDirection: "column",
        justifyContent: "flex-start",
        padding: 0,
      },
      "& a:first-child": {
        display: "none",
        [theme.breakpoints.up("md")]: {
          display: "flex",
          paddingBottom: theme.spacing(3),
          "& img": {
            maxHeight: theme.spacing(7),
          },
        },
      },
      "& a": {
        display: "flex",
        justifyContent: "center",
        textDecoration: "none",
        width: "fit-content",
        [theme.breakpoints.up("md")]: {
          width: theme.spacing(10),
        },
      },
    },
  }),
  {
    name: "Navigation",
  }
);

const Navigation = () => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <AppBar
        position="static"
        color="primary"
        elevation={0}
        classes={{ root: classes.root }}
        component="nav"
      >
        <Toolbar disableGutters className={classes.toolbar}>
          <Link href="/">
            <img src={logo} alt="Logo" />
          </Link>

          <Link href="/">
            <Button label="Home" icon="home" />
          </Link>

          {/* <Link href="/provide">
            <Button label="Provide" icon="plus-circle" />
          </Link>

          <Link href="/data">
            <Button label="Data" icon="database" />
          </Link> */}

          <Link href="/services">
            <Button label="Services" icon="cog" />
          </Link>

          <Link href="/provider">
            <Button label="Provider" icon="provider" />
          </Link>

          {/* <Link href="/marketplace">
            <Button label="Marketplace" icon="cart-full" />
          </Link> */}
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
};

export default Navigation;
