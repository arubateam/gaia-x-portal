import React from "react";
import Section from "./Section";
import ThumbCardImage from "./ThumbCardImage";
import { makeStyles, ExternalLink, PrimaryButton, Grid } from "./index";
const summit = "/images/gaia-x-summit.jpg";
const agenda = "/images/gaia-x-summit-agenda.jpg";
const infrastructure = "/images/infrastructure.jpg";

const useStyles = makeStyles((theme) => ({
  card: {
    background: "white",
    height: theme.spacing(77),
    justifyContent: "space-between",
    "& h4": {
      fontSize: theme.typography.size.xl,
    },
    "& p": {
      fontSize: theme.typography.size.m,
      fontWeight: theme.typography.fontWeightMedium,
    },
  },
}));

const AboutGAIAX = () => {
  const classes = useStyles();
  return (
    <Section name="About GAIA-X">
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6} md={4} lg={4}>
          <ThumbCardImage
            name="Pan-European GAIA-X Summit 2020"
            chipLabel="www.data-infrastructure.eu"
            excerpt="With GAIA-X, representatives from politics, business and science from France and Germany, together with other European partners, create a proposal for the next generation of a data infrastructure for Europe: a secure, federated system that meets the highest standards of digital sovereignty while promoting innovation. This project is the cradle of an open, transparent digital ecosystem, where data and services can be made available, collated and shared in an environment of trust."
            marketingImage={summit}
            hasShadow
            hasAvatar={false}
            hasChip={false}
            large
            className={classes.card}
            actions={
              <ExternalLink
                href="https://www.data-infrastructure.eu/GAIAX/Navigation/EN/Home/home.html"
                target="_blank"
                rel="noreferrer"
              >
                <PrimaryButton>Read More</PrimaryButton>
              </ExternalLink>
            }
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={4}>
          <ThumbCardImage
            name="GAIA-X Summit 2020 Agenda"
            excerpt="Join Peter Altmaier, the German Federal Minister of Economic Affairs and Energy, Bruno Le Maire, the French Minister of the Economy, Finance and the Recovery, Paola Pisano, the Italian Minister for Innovation and Digitalization and Thierry Breton, Commissioner of the European Commission (tbc), when they open the next phase of GAIA-X. Get a firsthand introduction to the initiative, benefit from and contribute to inspiring talks, and be part of the joint creation of GAIA-X. Meet other members..."
            marketingImage={agenda}
            hasShadow
            hasAvatar={false}
            hasChip={false}
            large
            className={classes.card}
            actions={
              <ExternalLink
                href="https://events.talque.com/gaia%E2%80%90x%E2%80%90summit/en/6iq6yI5LPSxaIRA6cmnq"
                target="_blank"
                rel="noreferrer"
              >
                <PrimaryButton>Read More</PrimaryButton>
              </ExternalLink>
            }
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={4}>
          <ThumbCardImage
            name="Une infrastructure numérique commune pour l’Europe"
            excerpt="Une plateforme européenne d’enregistrement de données dans des centres informatiques externes, voilà ce qui se cache derrière le projet GAIA-X dont l’objectif est de garantir performance, compétitivité, sécurité et fiabilité. Mais comment fonctionne GAIA-X, qui en bénéficie et de quelle manière? Principales questions et réponses."
            marketingImage={infrastructure}
            hasShadow
            hasAvatar={false}
            hasChip={false}
            large
            className={classes.card}
            actions={
              <ExternalLink
                href="https://www.bundesregierung.de/breg-fr/dossier/europe/gaia-x-1794698"
                target="_blank"
                rel="noreferrer"
              >
                <PrimaryButton>Read More</PrimaryButton>
              </ExternalLink>
            }
          />
        </Grid>
      </Grid>
    </Section>
  );
};

export default AboutGAIAX;
