import React from "react";
import { element, func } from "prop-types";
import Navigation from "./Navigation";

import {
  Fab,
  Container,
  makeStyles,
  useScrollTrigger,
  Fade,
  CircularProgress,
} from "./index";
const tree = "/images/gaia-x-tree.svg";

const useStyles = makeStyles((theme) => ({
  app: {
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh",
    overflowY: "hidden",
    [theme.breakpoints.up("md")]: {
      display: "grid",
      position: "relative",
      gridTemplateColumns: `calc(${
        theme.dimensions.navigationWidth
      }px + ${theme.spacing(2)}px) 1fr`,
      gridTemplateAreas: `
        "navigation header"
        "navigation main"`,
      gridTemplateRows: `${theme.spacing(11)}px 1fr`, // war 18
      background: `url(${tree})`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "702px 451px",
      backgroundPosition: "right bottom",
    },
  },
  navigationWrapper: {
    gridArea: "navigation",
  },
  headerWrapper: {
    height: theme.spacing(10),
    [theme.breakpoints.up("md")]: {
      gridArea: "header",
      height: "auto",
    },
  },
  main: {
    gridTemplateRows: "10px 1fr",
    gridArea: "main",
    [theme.breakpoints.up("md")]: {
      zIndex: 1,
      gridArea: "main",
      display: "grid",
      position: "relative",
      gridTemplateRows: "20px 1fr",
      gridTemplateColumns: "1fr",
      gridTemplateAreas: `"content"`,
    },
  },
  loading: {
    width: "40px !important",
    height: "40px !important",
  },
}));

function ScrollTop(props) {
  const { children, window } = props;

  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
    disableHysteresis: true,
    threshold: 100,
  });

  const handleClick = (event) => {
    const anchor = (event.target.ownerDocument || document).querySelector(
      "#back-to-top-anchor"
    );

    if (anchor) {
      anchor.scrollIntoView({ behavior: "smooth", block: "center" });
    }
  };

  return (
    <Fade in={trigger}>
      <div onClick={handleClick} role="presentation">
        {children}
      </div>
    </Fade>
  );
}

ScrollTop.propTypes = {
  children: element.isRequired,
  window: func,
};

const PersistGateLoading = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.app}>
      <>
        <div className={classes.navigationWrapper}>
          <Navigation />
        </div>
        <div className={classes.headerWrapper}>{/* <Header /> */}</div>
      </>

      <main className={classes.main}>
        <div id="back-to-top-anchor" />
        <Container maxWidth={false} disableGutters>
          <CircularProgress className={classes.loading} />
        </Container>
        <ScrollTop {...props}>
          <Fab />
        </ScrollTop>
      </main>
    </div>
  );
};

export default PersistGateLoading;
