import React from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: theme.spacing(14),
    height: theme.spacing(8),
    margin: theme.spacing(0, 2, 2, 0),
    backgroundColor: ({ selected }) =>
      selected ? theme.palette.primary[50] : theme.palette.background.paper,
    border: `1px solid ${theme.palette.primary[200]}`,
    borderRadius: theme.shape.borderRadius,
    transition: theme.transitions.create("background-color"),
    cursor: "pointer",
    "& label": {
      color: theme.palette.primary.main,
      marginTop: theme.spacing(0.5),
    },
    "& i": {
      color: theme.palette.primary[200],
      fontSize: 20,
    },
    "& img": {
      width: theme.spacing(7),
      height: "auto",
    },
    "&:hover": {
      backgroundColor: theme.palette.primary[50],
    },
  },
}));

export const Option = ({ value, label, icon, onClick, selected }) => {
  const classes = useStyles({ selected });
  return (
    <div className={classes.container} value={value} onClick={onClick}>
      {icon}
      {label && <label>{label}</label>}
    </div>
  );
};

export default Option;
