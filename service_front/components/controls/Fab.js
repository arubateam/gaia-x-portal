import React from "react";
import { any } from "prop-types";
import { Fab as MuiFab } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Icon from "./Icon";

const useStyles = makeStyles((theme) => ({
  label: {
    color: theme.palette.common.white,
  },
}));

const Fab = ({ style, ...rest }) => {
  const classes = useStyles();
  return (
    <MuiFab
      {...rest}
      color="secondary"
      aria-label="Scroll to Top"
      size="small"
      style={style}
      classes={{ label: classes.label }}
    >
      <Icon>chevron-up</Icon>
    </MuiFab>
  );
};

Fab.propTypes = {
  style: any,
};

export default Fab;
