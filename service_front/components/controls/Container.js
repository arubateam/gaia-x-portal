import React from "react";
import { oneOfType, arrayOf, string, any, node, bool } from "prop-types";
import { Container as MuiContainer } from "@material-ui/core";

const Container = ({
  classes,
  className,
  children,
  component,
  disableGutters,
  fixed,
  maxWidth,
}) => (
  <MuiContainer
    classes={classes}
    className={className}
    component={component}
    disableGutters={disableGutters}
    fixed={fixed}
    maxWidth={maxWidth}
  >
    {children}
  </MuiContainer>
);

Container.propTypes = {
  classes: string,
  className: string,
  children: any,
  component: node,
  disableGutters: bool,
  fixed: bool,
  maxWidth: oneOfType([arrayOf(any), bool, string]),
};

export default Container;
