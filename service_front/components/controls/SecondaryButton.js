import React, { forwardRef } from "react";
import { func, any } from "prop-types";
import { Button } from "@material-ui/core";
import { fade } from "@material-ui/core/styles/colorManipulator";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: theme.dimensions.inputHeight,
    letterSpacing: theme.typography.letterSpacing,
    "&:hover": {
      backgroundColor: fade(theme.palette.primary.main, 0.04),
    },
    "&:disabled": {
      color: theme.palette.text.disabled,
    },
    "& i": {
      marginRight: theme.spacing(1),
      fontSize: theme.typography.size.m,
    },
  },
}));

const SecondaryButton = forwardRef((props, ref) => {
  const { children, onClick, ...rest } = props;
  const classes = useStyles();
  return (
    <Button
      classes={{ root: classes.root }}
      color="primary"
      onClick={onClick}
      ref={ref}
      {...rest}
    >
      {children}
    </Button>
  );
});

SecondaryButton.propTypes = {
  children: any.isRequired,
  onClick: func,
};

export default SecondaryButton;
