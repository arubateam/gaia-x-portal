import React from "react";
import { object, element, string, func } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { IconButton as MuiIconButton } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    height: theme.spacing(5),
    width: theme.spacing(5),
  },
  sizeSmall: {
    height: theme.spacing(3),
    width: theme.spacing(3),
  },
}));

const IconButton = ({
  children,
  size,
  color,
  edge,
  onClick,
  style,
  ...rest
}) => {
  const classes = useStyles();
  return (
    <MuiIconButton
      classes={{ root: classes.root, sizeSmall: classes.sizeSmall }}
      size={size}
      color={color}
      edge={edge}
      onClick={onClick}
      style={style}
      {...rest}
    >
      {children}
    </MuiIconButton>
  );
};

IconButton.propTypes = {
  children: element.isRequired,
  size: string,
  color: string,
  edge: string,
  onClick: func,
  style: object,
};

IconButton.defaultPropTypes = {
  size: "medium",
  color: "primary",
  edge: false,
};

export default IconButton;
