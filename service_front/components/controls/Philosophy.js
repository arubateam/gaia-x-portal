import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Typography from "./Typography";
import Grid from "./Grid";
const consistencyImage = "/images/gaia-x-consistency.svg";
const simplicityImage = "/images/gaia-x-simplicity.svg";
const minimalismImage = "/images/gaia-x-minimalism.svg";
const feedbackImage = "/images/gaia-x-feedback.svg";

const useStyles = makeStyles((theme) => ({
  section: {
    padding: theme.spacing(5, 12, 12),
    borderRadius: theme.shape.borderRadius,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "right bottom",
    backgroundSize: "50%",
    [theme.breakpoints.only("md")]: {
      padding: theme.spacing(5, 5, 6, 6),
    },
    [theme.breakpoints.only("xs")]: {
      padding: theme.spacing(5),
    },
  },
  card: {
    marginBottom: 0,
    height: theme.spacing(16),
    alignItems: "center",
    justifyContent: "center",
    "& h4": {
      textAlign: "center",
      transition: theme.transitions.create("all"),
    },
    "&:hover": {
      "& h4": {
        fontSize: theme.typography.size.l,
      },
    },
  },
}));

const Philosophy = () => {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <Grid container spacing={3}>
      <Grid item xs={12} sm={6}>
        <section
          className={classes.section}
          style={{
            backgroundColor: theme.palette.primary[50],
            backgroundImage: `url(${consistencyImage})`,
          }}
        >
          <h2>Consistency</h2>
          <Typography variant="h4">
            Usability is enhanced when similar elements look consistent and show
            the functions they perform.
          </Typography>
        </section>
      </Grid>
      <Grid item xs={12} sm={6}>
        <section
          className={classes.section}
          style={{
            backgroundColor: theme.palette.neutral[400],
            backgroundImage: `url(${simplicityImage})`,
          }}
        >
          <h2>Simplicity</h2>
          <Typography variant="h4">
            A clear user interface is essential for users to reach their
            destination quickly and easily.
          </Typography>
        </section>
      </Grid>
      <Grid item xs={12} sm={6}>
        <section
          className={classes.section}
          style={{
            backgroundColor: theme.palette.neutral.main,
            backgroundImage: `url(${minimalismImage})`,
          }}
        >
          <h2>Minimalism</h2>
          <Typography variant="h4">
            Prevent users from having to navigate through unnecessary content.
            Concentrate on the essential.
          </Typography>
        </section>
      </Grid>
      <Grid item xs={12} sm={6}>
        <section
          className={classes.section}
          style={{
            backgroundColor: theme.palette.secondary[200],
            backgroundImage: `url(${feedbackImage})`,
          }}
        >
          <h2>Feedback</h2>
          <Typography variant="h4">
            Informing users about location, action, status changes or errors
            gives confidence in the system.
          </Typography>
        </section>
      </Grid>
    </Grid>
  );
};

export default Philosophy;
