import React from "react";
import { func, string } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { Chip } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    height: theme.spacing(3),
  },
  label: {
    color: theme.palette.common.white,
    fontWeight: theme.typography.fontWeightMedium,
    padding: theme.spacing(0, 1),
  },
  colorSecondary: {
    backgroundColor: theme.palette.primary[200],
  },
}));

const Amount = ({ label, onClick, onDelete, size, amount, ...rest }) => {
  const classes = useStyles();

  return (
    <Chip
      {...rest}
      label={label}
      size={size}
      onClick={onClick}
      onDelete={onDelete}
      color="secondary"
      classes={{
        root: classes.root,
        label: classes.label,
        colorSecondary: classes.colorSecondary,
      }}
    />
  );
};

Amount.propTypes = {
  label: string.isRequired,
  onClick: func,
  onDelete: func,
  size: string,
};

export default Amount;
