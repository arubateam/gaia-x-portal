import React from "react";
import { object, node, any, func, string } from "prop-types";
import { Tabs as MuiTabs } from "@material-ui/core/";

const Tabs = ({ children, classes, onChange, value, variant, ...rest }) => {
  return (
    <MuiTabs
      classes={classes}
      onChange={onChange}
      value={value}
      variant={variant}
      {...rest}
    >
      {children}
    </MuiTabs>
  );
};

Tabs.propTypes = {
  children: node,
  classes: object,
  onChange: func,
  value: any,
  variant: string,
};

export default Tabs;
