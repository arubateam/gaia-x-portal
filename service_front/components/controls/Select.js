import React, { createRef, useState } from "react";
import { string, func, node } from "prop-types";
import { TextField as MuiTextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  formControlRoot: {
    marginTop: 0,
    marginBottom: 0,
  },
  root: {
    borderRadius: theme.shape.borderRadius,
    "&:hover $notchedOutline": {
      borderColor: theme.palette.primary.main,
    },
  },
  input: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: theme.shape.borderRadius,
  },
  notchedOutline: {
    borderColor: theme.palette.primary[100],
    transition: theme.transitions.create("border-color"),
    borderRadius: theme.shape.borderRadius,
  },
  focused: {
    "& $notchedOutline": {
      // borderWidth: "1px !important",
      borderColor: theme.palette.primary.main,
    },
  },
  select: {
    borderRadius: theme.shape.borderRadius,
    "&:focus": {
      borderRadius: theme.shape.borderRadius,
      backgroundColor: theme.palette.background.paper,
    },
  },
  icon: {
    top: "calc(50% - 10px)",
    color: theme.palette.primary[200],
  },
}));

const Select = ({
  children,
  onChange,
  name,
  label,
  placeholder,
  className,
  value,
  ...rest
}) => {
  const ref = createRef();
  const classes = useStyles();

  return (
    <MuiTextField
      {...rest}
      select
      fullWidth
      name={name}
      label={label}
      placeholder={name}
      ref={ref}
      value={value}
      onChange={onChange}
      color="primary"
      variant="outlined"
      autoCorrect="off"
      autoCapitalize="off"
      spellCheck="false"
      classes={{ root: classes.formControlRoot }}
      InputProps={{
        classes: {
          root: classes.root,
          notchedOutline: classes.notchedOutline,
          input: classes.input,
          focused: classes.focused,
        },
      }}
      SelectProps={{
        classes: {
          select: classes.select,
          icon: classes.icon,
        },
      }}
    >
      {children}
    </MuiTextField>
  );
};

Select.propTypes = {
  children: node,
  placeholder: string,
  label: string,
  name: string,
  onChange: func,
  className: string,
  value: string,
};

export default Select;
