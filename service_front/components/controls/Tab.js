import React from "react";
import { object, bool, node, any } from "prop-types";
import { Tab as MuiTab } from "@material-ui/core/";

const Tab = ({ children, classes, disabled, icon, label, value, ...rest }) => {
  return (
    <MuiTab
      classes={classes}
      disabled={disabled}
      icon={icon}
      label={label}
      value={value}
      {...rest}
    >
      {children}
    </MuiTab>
  );
};

Tab.propTypes = {
  classes: object,
  disabled: bool,
  icon: node,
  label: node,
  value: any,
};

export default Tab;
