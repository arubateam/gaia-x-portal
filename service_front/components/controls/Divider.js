import React, { forwardRef } from "react";
import { string, bool } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { Divider as MuiDivider } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.primary[50],
  },
}));

const Divider = forwardRef((props, ref) => {
  const classes = useStyles();

  return (
    <MuiDivider
      ref={ref}
      variant={props.variant}
      orientation={props.orientation}
      className={props.className}
      flexItem={props.flexItem}
      classes={{ root: classes.root }}
      style={props.style}
    />
  );
});

Divider.propTypes = {
  variant: string,
  className: string,
  orientation: string,
  flexItem: bool,
};

Divider.defaultPropTypes = {
  variant: "fullWidth",
  orientation: "horizontal",
};

export default Divider;
