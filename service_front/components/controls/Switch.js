import React from "react";
import { bool, func, string } from "prop-types";
import { Switch as MuiSwitch } from "@material-ui/core/";

const Switch = ({ checked, onChange, name, ...rest }) => {
  return (
    <MuiSwitch
      {...rest}
      checked={checked}
      onChange={onChange}
      name={name}
      color="secondary"
      inputProps={{ "aria-label": "primary checkbox" }}
    />
  );
};

Switch.propTypes = {
  checked: bool,
  onChange: func,
  name: string,
};

export default Switch;
