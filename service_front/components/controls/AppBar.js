import React, { forwardRef } from "react";
import { string, node, elementType, number } from "prop-types";
import { AppBar as MuiAppBar } from "@material-ui/core/";
import { fade } from "@material-ui/core/styles/colorManipulator";
import { makeStyles } from "@material-ui/core/styles";
import Divider from "./Divider";

const useStyles = makeStyles((theme) => ({
  colorDefault: {
    backgroundColor: fade(theme.palette.neutral[50], 0.5),
    boxShadow: "none",
    backdropFilter: "blur(2px)",
  },
}));

const AppBar = forwardRef((props, ref) => {
  const classes = useStyles();
  return (
    <>
      <MuiAppBar
        ref={ref}
        classes={{ colorDefault: classes.colorDefault }}
        color={props.color}
        component={props.component}
        position={props.position}
        elevation={props.elevation}
        {...props}
      >
        {props.children}
      </MuiAppBar>
      {props.color === "default" && <Divider />}
    </>
  );
});

AppBar.propTypes = {
  children: node,
  color: string,
  component: elementType,
  position: string,
  elevation: number,
};

export default AppBar;
