import React from "react";
import {
  t,
  oneOf,
  oneOfType,
  arrayOf,
  any,
  string,
  bool,
  element,
  node,
} from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "./Paper";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    padding: theme.spacing(3),
    marginBottom: theme.spacing(2),
    boxShadow: ({ hasShadow }) =>
      hasShadow ? theme.dimensions.shadowCard : "none",
    "&:hover": {
      boxShadow: ({ hasHover, hasShadow }) => {
        if (hasHover && !hasShadow) return theme.dimensions.shadowCard;
        else if (hasHover && hasShadow) return theme.dimensions.shadowCardHover;
      },
    },
  },
  actionContainer: {
    display: "flex",
    justifyContent: "flex-end",
    whiteSpace: "nowrap",
    width: "100%",
  },
  link: {
    color: "transparent",
    textDecoration: "none",
  },
  primaryButton: {
    marginLeft: theme.spacing(1),
  },
}));

const Card = ({
  className,
  hasShadow,
  hasHover,
  children,
  actions,
  isProvider,
  kind,
  ...rest
}) => {
  const classes = useStyles({
    hasShadow,
    hasHover,
  });
  const kinds = {
    primary: "#003b46",
    secondary: "#00b2d1",
    orange: "#ffa726",
  };
  return (
    <Paper
      {...rest}
      style={{ borderTop: `8px solid ${kinds[kind]}` }}
      className={clsx(classes.container, className)}
    >
      {children}
      {actions}
    </Paper>
  );
};

Card.propTypes = {
  children: oneOfType([arrayOf(any), arrayOf(element), element, string]),
  className: string,
  actions: any,
  hasShadow: bool,
  hasHover: bool,
  isProvider: bool,
  kind: oneOf(["primary", "secondary", "orange"]),
};

Card.defaultProps = {
  hasShadow: true,
  hasHover: false,
  isProvider: false,
};

export default Card;
