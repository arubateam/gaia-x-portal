import React from "react";
import { string } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { ListItemText as MuiListItemText } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {},
}));

const ListItemText = ({ className, ...rest }) => {
  const classes = useStyles();

  return (
    <MuiListItemText
      className={className}
      classes={{ root: classes.root }}
      {...rest}
    />
  );
};

ListItemText.propTypes = {
  className: string,
};

export default ListItemText;
