import React, { createRef, useState } from "react";
import { string, func } from "prop-types";
import { TextField as MuiTextField, MenuItem } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Label from "./Label";

const useStyles = makeStyles((theme) => ({
  select: {
    backgroundColor: "transparent",
    overflow: "initial",
    "&:focus": {
      backgroundColor: "transparent",
    },
  },
  inputMarginDense: {
    padding: 0,
  },
  root: {
    color: theme.palette.secondary.main,
    "& svg": {
      fill: theme.palette.secondary.main,
    },
  },
  flex: {
    display: "flex",
    alignItems: "center",
    "& > div": {
      margin: theme.spacing(0, 0, 0, 1),
    },
  },
}));

const options = [
  {
    value: "New",
    label: "New",
  },
  {
    value: "AtoZ",
    label: "A to Z",
  },
  {
    value: "ZtoA",
    label: "Z to A",
  },
];

const SortBy = ({ onChange, name, label, placeholder, className, ...rest }) => {
  const [option, setOption] = useState("New");
  const ref = createRef();
  const classes = useStyles();

  const handleChange = (event) => {
    setOption(event.target.value);
  };
  return (
    <div className={classes.flex}>
      <Label>Sort By</Label>
      <MuiTextField
        {...rest}
        select
        value={option}
        onChange={handleChange}
        name={name}
        label={label}
        placeholder={placeholder}
        color="primary"
        variant="standard"
        autoCorrect="off"
        autoCapitalize="off"
        spellCheck="false"
        ref={ref}
        margin="dense"
        InputProps={{
          disableUnderline: true,
          classes: {
            inputMarginDense: classes.inputMarginDense,
            root: classes.root,
          },
        }}
        SelectProps={{
          classes: {
            select: classes.select,
          },
        }}
      >
        {options.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </MuiTextField>
    </div>
  );
};

SortBy.propTypes = {
  placeholder: string,
  label: string,
  name: string,
  onChange: func,
  className: string,
};

export default SortBy;
