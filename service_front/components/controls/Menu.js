import React, { forwardRef } from "react";
import { string } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { Menu as MuiMenu } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  paper: {
    boxShadow: theme.dimensions.shadowPopover,
    borderRadius: theme.shape.borderRadius,
  },
  list: {
    "& li": {
      borderBottom: `1px solid ${theme.palette.primary[50]}`,
      "&:last-child": {
        borderBottom: 0,
      },
    },
  },
}));

const Menu = forwardRef((props, ref) => {
  const classes = useStyles();

  return (
    <MuiMenu
      ref={ref}
      classes={{ paper: classes.paper, list: classes.list }}
      className={props.className}
      {...props}
    />
  );
});

Menu.propTypes = {
  className: string,
};

export default Menu;
