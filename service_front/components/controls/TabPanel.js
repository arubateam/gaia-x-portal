import React from "react";
import { node, any, bool } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  tabpanel: {
    "& > div": {
      padding: ({ paddingBottom }) =>
        paddingBottom ? theme.spacing(3) : theme.spacing(3, 3, 0, 3),
    },
  },
}));

const TabPanel = ({ children, value, index, paddingBottom, ...rest }) => {
  const classes = useStyles({ paddingBottom });
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      className={classes.tabpanel}
      {...rest}
    >
      {value === index && <div>{children}</div>}
    </div>
  );
};

TabPanel.propTypes = {
  children: node,
  index: any,
  value: any,
  paddingBottom: bool,
};

TabPanel.defaultProps = {
  paddingBottom: true,
};

export default TabPanel;
