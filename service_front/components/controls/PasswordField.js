import React, { createRef } from "react";
import { string, func, bool } from "prop-types";
import { InputAdornment, TextField as MuiTextField } from "@material-ui/core";
import IconButton from "./IconButton";
import Icon from "./Icon";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  formControlRoot: {
    marginTop: 0,
    marginBottom: 0,
  },
  root: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: theme.shape.borderRadius,
    "&:hover $notchedOutline": {
      borderColor: ({ error, success, disabled }) => {
        if (error === true) return theme.palette.error.main;
        else if (success === true) return theme.palette.success.main;
        else if (disabled === true) return theme.palette.action.disabled;
        return theme.palette.primary.main;
      },
    },
  },
  input: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: theme.shape.borderRadius,
  },

  notchedOutline: {
    borderColor: ({ error, success }) => {
      if (error === true) return theme.palette.error.main;
      else if (success === true) return theme.palette.success.main;
      return theme.palette.primary[100];
    },
    transition: theme.transitions.create("border-color"),
    borderRadius: theme.shape.borderRadius,
  },
  focused: {
    "& $notchedOutline": {
      borderColor: ({ error, success }) => {
        if (error === true) return theme.palette.error.main;
        else if (success === true) return theme.palette.success.main;
        return theme.palette.primary.main;
      },
    },
  },
}));

const PasswordField = ({
  onChange,
  name,
  label,
  placeholder,
  className,
  error,
  success,
  disabled,
  fullWidth,
  ...rest
}) => {
  const ref = createRef();
  const classes = useStyles();
  const [values, setValues] = React.useState({
    password: "",
    showPassword: false,
  });

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  return (
    <MuiTextField
      {...rest}
      type={values.showPassword ? "text" : "password"}
      onChange={onChange}
      name={name}
      label={label}
      placeholder={placeholder}
      fullWidth={fullWidth}
      ref={ref}
      color="primary"
      variant="outlined"
      autoCorrect="off"
      autoCapitalize="off"
      spellCheck="false"
      classes={{ root: classes.formControlRoot }}
      InputProps={{
        classes: {
          root: classes.root,
          notchedOutline: classes.notchedOutline,
          input: classes.input,
          focused: classes.focused,
        },
        endAdornment: (
          <InputAdornment position="end">
            <IconButton
              edge="end"
              size="small"
              color="secondary"
              aria-label="toggle password visibility"
              onClick={handleClickShowPassword}
              onMouseDown={handleMouseDownPassword}
            >
              {values.showPassword ? (
                <Icon style={{ marginLeft: 16 }}>eye-close</Icon>
              ) : (
                <Icon>eye</Icon>
              )}
            </IconButton>
          </InputAdornment>
        ),
      }}
    ></MuiTextField>
  );
};

PasswordField.propTypes = {
  placeholder: string,
  label: string,
  name: string,
  onChange: func,
  className: string,
  error: bool,
  success: bool,
  fullWidth: bool,
  disabled: bool,
};

PasswordField.defaultProps = {
  fullWidth: true,
  error: false,
  success: false,
  disabled: false,
};

export default PasswordField;
