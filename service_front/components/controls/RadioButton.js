import React from "react";
import { string, bool, func } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { Radio as MuiRadio } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    color: theme.palette.primary[100],
    transition: theme.transitions.create("color"),
    "&:hover": {
      color: theme.palette.primary.main,
    },
  },
}));

const RadioButton = ({ checked, onChange, value, name, ...rest }) => {
  const classes = useStyles();
  return (
    <MuiRadio
      {...rest}
      checked={checked}
      onChange={onChange}
      value={value}
      name={name}
      color="primary"
      size="small"
      inputProps={{ "aria-label": "A" }}
      classes={{ root: classes.root }}
    />
  );
};

RadioButton.propTypes = {
  checked: bool,
  onChange: func,
  name: string,
  value: string,
};

export default RadioButton;
