import React from "react";
import { string } from "prop-types";
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: theme.shape.borderRadius,
    "&:hover $notchedOutline": {
      borderColor: theme.palette.primary.main,
    },
  },
  notchedOutline: {
    borderColor: theme.palette.primary[100],
    transition: theme.transitions.create("border-color"),
    borderRadius: theme.shape.borderRadius,
  },
}));

const options = [
  { title: "Option 1" },
  { title: "Option 2" },
  { title: "Option 3" },
];

const ComboBox = ({ label, ...rest }) => {
  const classes = useStyles();
  return (
    <Autocomplete
      {...rest}
      fullWidth
      id="combo-box"
      options={options}
      getOptionLabel={(option) => option.title}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          variant="outlined"
          InputProps={{
            ...params.InputProps,
            classes: {
              root: classes.root,
              notchedOutline: classes.notchedOutline,
            },
          }}
        />
      )}
    />
  );
};

ComboBox.propTypes = {
  label: string.isRequired,
};

export default ComboBox;
