import React from "react";
import { object, string, bool, any } from "prop-types";
import { FormControlLabel as MuiFormControlLabel } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  label: {
    marginBottom: 1,
  },
});

const FormControlLabel = ({ value, control, label, checked, className }) => {
  const classes = useStyles();
  return (
    <MuiFormControlLabel
      value={value}
      control={control}
      label={label}
      checked={checked}
      className={className}
      classes={{ label: classes.label }}
      labelPlacement="end"
    />
  );
};

FormControlLabel.propTypes = {
  value: any,
  label: string.isRequired,
  control: object,
  checked: bool,
  className: string,
};

export default FormControlLabel;
