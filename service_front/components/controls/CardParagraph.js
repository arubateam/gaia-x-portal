import React from "react";
import { string, bool } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  paragraph: {
    marginBottom: ({ margin }) => (margin ? theme.spacing(2) : 0),
  },
}));

const CardParagraph = ({ children, margin }) => {
  const classes = useStyles({ margin });
  return (
    <Typography variant="h2" className={classes.paragraph}>
      {children}
    </Typography>
  );
};

CardParagraph.propTypes = {
  children: string,
  margin: bool,
};

CardParagraph.defaultProps = {
  margin: true,
};

export default CardParagraph;
