import React, { forwardRef } from "react";
import { string } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { MenuItem as MuiMenuItem } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    "&$selected": {
      backgroundColor: theme.palette.neutral.main,
      "&:hover": {
        backgroundColor: theme.palette.neutral.dark,
      },
    },
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(0.5),
    "&:hover": {
      backgroundColor: theme.palette.neutral[100],
      "& svg": {
        fill: theme.palette.primary.light,
      },
    },
  },
  selected: {},
}));

const MenuItem = forwardRef((props, ref) => {
  const classes = useStyles();

  return (
    <MuiMenuItem
      ref={ref}
      className={props.className}
      ListItemClasses={{
        root: classes.root,
        selected: classes.selected,
      }}
      {...props}
    />
  );
});

MenuItem.propTypes = {
  className: string,
};

export default MenuItem;
