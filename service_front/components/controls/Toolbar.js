import React from "react";
import { object, string, node, elementType, bool } from "prop-types";
import { Toolbar as MuiToolbar } from "@material-ui/core/";

const Toolbar = React.forwardRef((props, ref) => {
  return (
    <MuiToolbar
      ref={ref}
      classes={props.classes}
      color={props.color}
      component={props.component}
      disableGutters={props.disableGutters}
      variant={props.variant}
      {...props}
    >
      {props.children}
    </MuiToolbar>
  );
});

Toolbar.propTypes = {
  children: node,
  classes: object,
  color: string,
  component: elementType,
  disableGutters: bool,
  variant: string,
};

export default Toolbar;
