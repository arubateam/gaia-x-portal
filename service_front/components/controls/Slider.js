import React from "react";
import { any, number, func, string, bool } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { Slider as MuiSlider } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2, 0),
  },
  valueLabel: {
    "& *": {
      background: "transparent",
      color: theme.palette.text.primary,
    },
  },
  thumb: {
    "& > span": {
      top: "-18px",
    },
  },
}));

const Slider = ({
  defaultValue,
  getAriaValueText,
  valueLabelDisplay,
  value,
  step,
  marks,
  min,
  max,
  onChange,
}) => {
  const classes = useStyles();

  return (
    <MuiSlider
      defaultValue={defaultValue}
      getAriaValueText={getAriaValueText}
      valueLabelDisplay={valueLabelDisplay}
      value={value}
      classes={{
        root: classes.root,
        valueLabel: classes.valueLabel,
        thumb: classes.thumb,
      }}
      step={step}
      marks={marks}
      min={min}
      max={max}
      onChange={onChange}
      color="secondary"
    />
  );
};

Slider.propTypes = {
  defaultValue: number,
  getAriaValueText: func,
  valueLabelDisplay: string,
  value: any, // should be changes into number or array of number
  step: number,
  marks: bool,
  min: number,
  max: number,
  onChange: func,
};

export default Slider;
