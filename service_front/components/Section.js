import React, { useState } from "react";
import {
  any,
  arrayOf,
  bool,
  element,
  func,
  oneOfType,
  string,
} from "prop-types";

import {
  makeStyles,
  Typography,
  ArrowBackIcon,
  IconButton,
  SortBy,
  FilterListIcon,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  useMediaQuery,
  PrimaryButton,
  SecondaryButton,
  useTheme,
} from "./index";

import Filter from "./Filter";

const useStyles = makeStyles((theme) => ({
  container: {
    marginBottom: 0,
    [theme.breakpoints.up("md")]: {
      marginBottom: theme.spacing(4),
    },
  },
  header: {
    alignItems: "center",
    display: "flex",
    justifyContent: "space-between",
    marginBottom: theme.spacing(1),
  },
  flex: {
    alignItems: "center",
    display: "flex",
    "& > div > button": {
      marginRight: theme.spacing(1),
    },
  },
  mobileFilter: {
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
}));

const Section = ({
  children,
  className,
  hasNavigation,
  name,
  onGoBack,
  hasSortBy,
  hasFilter,
  isH2,
}) => {
  const classes = useStyles({ hasFilter });
  const [open, setOpen] = useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const Component = hasNavigation ? "div" : "div";

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <section className={classes.container}>
      <div className={classes.header}>
        <div className={classes.flex}>
          <div onClick={onGoBack} style={{ display: !onGoBack && "none" }}>
            <IconButton size="small">
              <ArrowBackIcon color="secondary" />
            </IconButton>
          </div>
          {isH2 ? (
            <Typography variant="h2">{name}</Typography>
          ) : (
            <Typography variant="h1">{name}</Typography>
          )}
        </div>
        <div className={classes.flex}>
          {hasFilter && (
            <div className={classes.mobileFilter}>
              <IconButton
                aria-label="show filter"
                aria-controls="filter"
                onClick={handleClickOpen}
              >
                <FilterListIcon color="secondary" />
              </IconButton>
              <Dialog
                fullScreen={fullScreen}
                open={open}
                onClose={handleClose}
                aria-labelledby="filter-dialog"
              >
                <DialogTitle id="filter-dialog">
                  <Typography variant="h1" component="span">
                    Filter
                  </Typography>
                </DialogTitle>
                <DialogContent>
                  <Filter />
                </DialogContent>
                <DialogActions>
                  <SecondaryButton onClick={handleClose}>
                    Cancel
                  </SecondaryButton>
                  <PrimaryButton onClick={handleClose}>Apply</PrimaryButton>
                </DialogActions>
              </Dialog>
            </div>
          )}
          {/* {hasSortBy && <SortBy label="" />} */}
          {hasSortBy && <></>}
        </div>
      </div>
      <Component className={className}>{children}</Component>
    </section>
  );
};

Section.propTypes = {
  children: oneOfType([element, arrayOf(any), string]),
  className: string,
  hasNavigation: bool,
  name: string,
  onGoBack: func,
  hasSortBy: bool,
  hasFilter: bool,
  isH2: bool,
};

Section.defaultProps = {
  hasNavigation: false,
  hasSortBy: false,
  hasFilter: false,
  isH2: false,
};

export default Section;
