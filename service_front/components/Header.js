import React, { useState } from "react";
import { element, func } from "prop-types";

import {
  useScrollTrigger,
  Slide,
  AccountCircleIcon,
  VpnKeyIcon,
  PersonAddIcon,
  DescriptionIcon,
  MoreIcon,
  ShoppingBasketIcon,
  HelpIcon,
  ExitToAppIcon,
  LiveHelpIcon,
  PrimaryButton,
  SecondaryButton,
  IconButton,
  AppBar,
  Toolbar,
  Menu,
  MenuItem,
  ListItemIcon,
  ListItemText,
  Avatar,
  Icon,
  Link,
  makeStyles,
  Link as ExternalLink,
} from "./index";

const logo = "/images/gaia-x-logo-blue.svg";
const companyLogo = "/images/company-logo.png";

const useStyles = makeStyles((theme) => ({
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: theme.spacing(1),
      backgroundColor: theme.palette.neutral[50],
      borderRadius: 50,
      boxShadow: theme.dimensions.shadowCard,
    },
    "& button": {
      margin: theme.spacing(0, 1),
      "& i": {
        fontSize: 20,
      },
    },
  },
  sectionMobile: {
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
  appBar: {
    top: theme.spacing(2),
    left: theme.spacing(2),
    width: `calc(100% - ${theme.spacing(4)}px)`,
    [theme.breakpoints.up("md")]: {
      right: theme.spacing(2),
    },
  },
  toolbar: {
    display: "flex",
    justifyContent: "space-between",
    minHeight: 0,
    [theme.breakpoints.up("md")]: {
      justifyContent: "flex-end",
    },
  },
  logo: {
    width: 26,
    height: theme.spacing(5),
  },
  avatar: {
    padding: 0,
  },
  link: {
    display: "flex",
    alignItems: "center",
  },
  moreIcon: {
    backgroundColor: theme.palette.neutral[50],
    borderRadius: 50,
    boxShadow: theme.dimensions.shadowCard,
  },
}));

function HideOnScroll(props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({ target: window ? window() : undefined });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

HideOnScroll.propTypes = {
  children: element.isRequired,
  window: func,
};

const Header = (props) => {
  const classes = useStyles();
  const [supportAnchorEl, setSupportAnchorEl] = useState(null);
  const [mobileAnchorEl, setMobileAnchorEl] = useState(null);
  const [accountAnchorEl, setAccountAnchorEl] = useState(null);

  const isMenuOpen = Boolean(supportAnchorEl);
  const isMobileMenuOpen = Boolean(mobileAnchorEl);
  const isAccountMenuOpen = Boolean(accountAnchorEl);

  const handleSupportMenuOpen = (event) => {
    setSupportAnchorEl(event.currentTarget);
  };
  const handleAccountMenuOpen = (event) => {
    setAccountAnchorEl(event.currentTarget);
  };
  const handleMobileMenuOpen = (event) => {
    setMobileAnchorEl(event.currentTarget);
  };
  const handleSupportMenuClose = () => {
    setSupportAnchorEl(null);
  };
  const handleMobileMenuClose = () => {
    setMobileAnchorEl(null);
  };
  const handleAccountMenuClose = () => {
    setAccountAnchorEl(null);
  };

  const menuId = "support-menu";
  const renderSupportMenu = (
    <Menu
      anchorEl={supportAnchorEl}
      id={menuId}
      keepMounted
      open={isMenuOpen}
      onClose={handleSupportMenuClose}
    >
      <MenuItem>
        <Link
          href="/"
          onClick={handleSupportMenuClose}
          className={classes.link}
        >
          <ListItemIcon>
            <Icon>question-circle</Icon>
          </ListItemIcon>
          <ListItemText primary="Support" />
        </Link>
      </MenuItem>
      <MenuItem>
        <Link
          href="/"
          onClick={handleSupportMenuClose}
          className={classes.link}
        >
          <ListItemIcon>
            <Icon>radio-button</Icon>
          </ListItemIcon>
          <ListItemText primary="FAQ" />
        </Link>
      </MenuItem>
    </Menu>
  );

  const accountMenuId = "account-menu";
  const renderAccountMenu = (
    <Menu
      anchorEl={accountAnchorEl}
      id={accountMenuId}
      keepMounted
      open={isAccountMenuOpen}
      onClose={handleAccountMenuClose}
    >
      <MenuItem>
        <Link
          href="/my-account"
          onClick={handleAccountMenuClose}
          className={classes.link}
        >
          <ListItemIcon>
            <Icon>user</Icon>
          </ListItemIcon>
          <ListItemText primary="My Account" />
        </Link>
      </MenuItem>
      <MenuItem>
        <Link
          href="/"
          onClick={handleAccountMenuClose}
          className={classes.link}
        >
          <ListItemIcon>
            <Icon>exit</Icon>
          </ListItemIcon>
          <ListItemText primary="Sign out" />
        </Link>
      </MenuItem>
    </Menu>
  );

  const mobileMenuId = "mobile-menu";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileAnchorEl}
      id={mobileMenuId}
      keepMounted
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <Link
          href="/register"
          onClick={handleMobileMenuClose}
          className={classes.link}
        >
          <ListItemIcon>
            <Icon>add-user</Icon>
          </ListItemIcon>
          <ListItemText primary="Register" />
        </Link>
      </MenuItem>
      <MenuItem>
        <Link
          href="/login"
          onClick={handleMobileMenuClose}
          className={classes.link}
        >
          <ListItemIcon>
            <Icon>enter</Icon>
          </ListItemIcon>
          <ListItemText primary="Login" />
        </Link>
      </MenuItem>
      <MenuItem>
        <Link
          href="/my-account"
          onClick={handleMobileMenuClose}
          className={classes.link}
        >
          <ListItemIcon>
            <Icon>user</Icon>
          </ListItemIcon>
          <ListItemText primary="My Account" />
        </Link>
      </MenuItem>
      <MenuItem>
        <Link href="/" onClick={handleMobileMenuClose} className={classes.link}>
          <ListItemIcon>
            <Icon>cart</Icon>
          </ListItemIcon>
          <ListItemText primary="Cart" />
        </Link>
      </MenuItem>
      <MenuItem>
        <Link href="/" onClick={handleMobileMenuClose} className={classes.link}>
          <ListItemIcon>
            <Icon>question-circle</Icon>
          </ListItemIcon>
          <ListItemText primary="Support" />
        </Link>
      </MenuItem>
      <MenuItem>
        <Link href="/" onClick={handleMobileMenuClose} className={classes.link}>
          <ListItemIcon>
            <Icon>radio-button</Icon>
          </ListItemIcon>
          <ListItemText primary="FAQ" />
        </Link>
      </MenuItem>
      <MenuItem>
        <Link href="/" onClick={handleMobileMenuClose} className={classes.link}>
          <ListItemIcon>
            <Icon>exit</Icon>
          </ListItemIcon>
          <ListItemText primary="Sign out" />
        </Link>
      </MenuItem>
    </Menu>
  );

  return (
    <>
      <HideOnScroll {...props}>
        <AppBar
          position="fixed"
          color="transparent"
          elevation={0}
          className={classes.appBar}
        >
          <Toolbar
            className={classes.toolbar}
            disableGutters
            component="header"
          >
            <div className={classes.sectionDesktop}>
              <Link href="/register">
                <SecondaryButton>Register</SecondaryButton>
              </Link>
              {/* <ExternalLink href="/login">
                <PrimaryButton>Login</PrimaryButton>
              </ExternalLink> */}
              {/* <IconButton
                aria-controls={accountMenuId}
                aria-label="My Account"
                className={classes.avatar}
                onClick={handleAccountMenuOpen}
              >
                <Avatar src={companyLogo} alt="My Avatar" />
              </IconButton>
              <IconButton
                aria-label="Cart"
                color="primary"
                style={{ margin: "0px 4px" }}
              >
                <Icon>cart</Icon>
              </IconButton>
              <IconButton
                edge="end"
                aria-label="support menu"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleSupportMenuOpen}
                color="primary"
                style={{ margin: "0px 4px" }}
              >
                <Icon>more-vertical</Icon>
              </IconButton> */}
            </div>
            <div className={classes.sectionMobile}>
              <Link href="/" className={classes.logo}>
                <img src={logo} alt="GAIA-X Logo" />
              </Link>
              <IconButton
                aria-label="show more"
                aria-controls={mobileMenuId}
                aria-haspopup="true"
                onClick={handleMobileMenuOpen}
                color="primary"
                className={classes.moreIcon}
              >
                <Icon>more-vertical</Icon>
              </IconButton>
            </div>
          </Toolbar>
        </AppBar>
      </HideOnScroll>
      {renderAccountMenu}
      {renderMobileMenu}
      {renderSupportMenu}
    </>
  );
};

export default Header;
