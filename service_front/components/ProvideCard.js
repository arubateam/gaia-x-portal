import React from "react";
import { any, string, node } from "prop-types";
import { Card, makeStyles, Typography, Icon } from "./index";

const useStyles = makeStyles((theme) => ({
  card: {
    flexDirection: "column",
    marginBottom: 0,
  },
  flex: {
    display: "flex",
    alignItems: "center",
    marginBottom: theme.spacing(2),
  },
  icon: {
    color: theme.palette.primary[200],
    fontSize: theme.spacing(5),
    marginRight: theme.spacing(2),
  },
  headline: {
    fontSize: theme.typography.size.l,
    fontWeight: theme.typography.fontWeightMedium,
  },
  description: {
    marginBottom: theme.spacing(3),
  },
}));

const ProvideCard = ({ icon, headline, description, actions, ...rest }) => {
  const classes = useStyles();
  return (
    <Card
      {...rest}
      hasShadow={false}
      variant="outlined"
      className={classes.card}
      actions={actions}
    >
      <div className={classes.flex}>
        <Icon className={classes.icon}>{icon}</Icon>
        <Typography
          variant="h2"
          color="textPrimary"
          className={classes.headline}
        >
          {headline}
        </Typography>
      </div>
      <Typography
        variant="body2"
        color="textPrimary"
        className={classes.description}
      >
        {description}
      </Typography>
    </Card>
  );
};

ProvideCard.propTypes = {
  icon: node,
  headline: string,
  description: string,
  actions: any,
};

export default ProvideCard;
