import React from "react";
import clsx from "clsx";
import { any, string, node, bool, func } from "prop-types";
import { makeStyles, Card, Avatar, Chip, Typography, Tooltip } from "./index";
const companyLogo = "/images/company-logo.png";

const useStyles = makeStyles((theme) => ({
  card: {
    flexDirection: "column",
    alignItems: "flex-end",
    padding: ({ large }) => (large ? theme.spacing(3) : theme.spacing(2)),
    background: `linear-gradient(to bottom right, ${theme.palette.neutral[50]}, ${theme.palette.neutral[100]})`,
  },
  flex: {
    display: "flex",
    alignItems: "center",
    marginBottom: theme.spacing(2),
  },
  icon: {
    height: theme.spacing(7),
    width: theme.spacing(7),
    marginBottom: theme.spacing(3),
    "& svg": {
      fontSize: theme.spacing(7),
      fill: theme.palette.warning.main,
      marginRight: theme.spacing(2),
    },
  },
  avatar: {
    marginRight: theme.spacing(2),
  },
  name: {
    // whiteSpace: "nowrap",
    // maxWidth: theme.spacing(20),
    // width: theme.spacing(20),
    // textOverflow: "ellipsis",
    // overflow: "hidden",
    // [theme.breakpoints.down("md")]: {
    //   maxWidth: theme.spacing(20),
    //   width: theme.spacing(20),
    // },
  },
  excerpt: {
    color: theme.palette.neutral[1000],
    marginBottom: theme.spacing(2),
  },
  chipContainer: {
    marginBottom: theme.spacing(2),
    display: "flex",
  },
  thumb: {
    borderRadius: theme.shape.borderRadius,
    height: ({ large }) => (large ? theme.spacing(19) : theme.spacing(12)),
    marginBottom: theme.spacing(2),
  },
  backgroundImage: {
    padding: theme.spacing(3),
    borderRadius: theme.shape.borderRadius,
    height: "100%",
    backgroundRepeat: "no-repeat",
    backgroundImage: ({ marketingImage }) => `url(${marketingImage})`,
    backgroundSize: "cover",
    backgroundPosition: "center",
    transition: theme.transitions.create("background-size", {
      duration: theme.transitions.duration.complex,
      easing: theme.transitions.easing.easeIn,
    }),
  },
}));

const ThumbCardImage = ({
  icon,
  name,
  chipLabel,
  excerpt,
  onClick,
  isProvider,
  marketingImage,
  hasShadow,
  actions,
  large,
  hasAvatar,
  hasChip,
  className,
  ...rest
}) => {
  const classes = useStyles({ marketingImage, large });
  return (
    <Card
      {...rest}
      actions={actions}
      className={clsx(classes.card, className)}
      hasShadow={hasShadow}
      large={large ? 1 : 0}
    >
      <div>
        <div className={classes.thumb}>
          <div className={classes.backgroundImage}></div>
        </div>
        <div className={classes.flex}>
          {hasAvatar && (
            <Avatar
              src={companyLogo}
              alt="Company Logo"
              className={classes.avatar}
              size="large"
              boxShadow
            />
          )}
          <div className={classes.item}>
            <Tooltip title={name}>
              <div>
                <Typography variant="h4" className={classes.name}>
                  {name}
                </Typography>
              </div>
            </Tooltip>
            {hasChip && (
              <Chip label={chipLabel} size="small" onClick={onClick} hasIcon />
            )}
          </div>
        </div>
        <div>
          <Typography variant="body2" className={classes.excerpt}>
            {excerpt}
          </Typography>
        </div>
      </div>
    </Card>
  );
};

ThumbCardImage.propTypes = {
  icon: node,
  chipLabel: string,
  excerpt: string,
  OnClick: func,
  actions: any,
  isProvider: bool,
  large: bool,
  hasAvatar: bool,
  hasChip: bool,
  className: string,
};

ThumbCardImage.defaultProps = {
  isProvider: false,
  hasAvatar: true,
  hasChip: true,
};

export default ThumbCardImage;
