import React from "react";
import { func, any, bool } from "prop-types";

import {
  Typography,
  makeStyles,
  Link,
  Card,
  TextField,
  PasswordField,
  Checkbox,
  Hyperlink,
  FormControlLabel,
  PrimaryButton,
  Grid,
} from "./index";
import CardAside from "./CardAside";
const logo = "/images/gaia-x-logo-blue.svg";

const useStyles = makeStyles((theme) => ({
  card: {
    flexDirection: "column",
    width: theme.spacing(48),
    boxShadow: theme.dimensions.shadowDialog,
  },
  cardAside: {
    marginBottom: theme.spacing(3),
  },
  header: {
    display: "flex",
    "& img": {
      width: 35,
      marginRight: theme.spacing(2),
    },
  },
  headline: {
    display: "flex",
    flexDirection: "column",
    "& h1": {
      fontSize: theme.typography.size.xxl,
      color: theme.palette.primary[900],
    },
    "& h2": {
      fontSize: theme.typography.size.xl,
      color: theme.palette.primary.light,
    },
  },
  primaryButton: {
    marginBottom: theme.spacing(2),
  },
  actions: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: theme.spacing(2),
  },
  account: {
    textAlign: "center",
  },
  registerLink: {
    marginLeft: theme.spacing(1),
  },
}));

const LoginForm = ({
  onChangeTextfield,
  onChangePassword,
  onClickPrimaryButton,
  checked,
  onChangeCheckbox,
  forgotPasswordLink,
  registerLink,
}) => {
  const classes = useStyles();
  return (
    <Card className={classes.card}>
      <CardAside className={classes.cardAside}>
        <div className={classes.header}>
          <img src={logo} alt="Logo" />
          <div className={classes.headline}>
            <Typography variant="h1" component="h1">
              Welcome to GAIA-X
            </Typography>
            <Typography variant="h2" component="h2">
              sign in to continue
            </Typography>
          </div>
        </div>
      </CardAside>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <TextField label="Email Address" onChange={onChangeTextfield} />
        </Grid>
        <Grid item xs={12}>
          <PasswordField label="Password" onChange={onChangePassword} />
        </Grid>
        <Grid item xs={12}>
          <Link href="/">
            <PrimaryButton
              dark
              fullWidth
              className={classes.primaryButton}
              onClick={onClickPrimaryButton}
            >
              Login
            </PrimaryButton>
          </Link>
        </Grid>
      </Grid>
      <div className={classes.actions}>
        <FormControlLabel
          control={
            <Checkbox
              checked={checked}
              onChange={onChangeCheckbox}
              name="checked"
              color="primary"
            />
          }
          label="Remember Me"
        />
        <Link href={forgotPasswordLink}>
          <Hyperlink>Forgot Password?</Hyperlink>
        </Link>
      </div>
      <Typography variant="body2" className={classes.account}>
        Dont't have any account yet?
        <Link href={registerLink}>
          <Hyperlink className={classes.registerLink}>Register now.</Hyperlink>
        </Link>
      </Typography>
    </Card>
  );
};

LoginForm.propTypes = {
  onChangeTextfield: func,
  onChangePassword: func,
  onClickPrimaryButton: func,
  checked: bool,
  onChangeCheckbox: func,
  forgotPasswordLink: any,
  registerLink: any,
};

export default LoginForm;
