import React from "react";
import { any, string, node, bool, func } from "prop-types";
import { makeStyles, Typography, Card, Avatar, Chip } from "./index";
const companyLogo = "/images/company-logo.png";

const useStyles = makeStyles((theme) => ({
  card: {
    flexDirection: "column",
    alignItems: "flex-end",
  },
  flex: {
    display: "flex",
    alignItems: "center",
    marginBottom: theme.spacing(2),
  },
  icon: {
    height: theme.spacing(7),
    width: theme.spacing(7),
    marginBottom: theme.spacing(3),
    "& svg": {
      fontSize: theme.spacing(7),
      fill: theme.palette.warning.main,
      marginRight: theme.spacing(2),
    },
  },
  avatar: {
    height: theme.spacing(7),
    width: theme.spacing(7),
    marginBottom: theme.spacing(3),
  },
  name: {
    fontSize: theme.typography.size.xl,
    fontWeight: theme.typography.fontWeightMedium,
    marginBottom: theme.spacing(1),
    whiteSpace: "nowrap",
  },
  excerpt: {
    color: theme.palette.neutral[1000],
    marginBottom: theme.spacing(2),
  },
  chipContainer: {
    marginBottom: theme.spacing(2),
    display: "flex",
  },
}));

const ThumbCard = ({
  color,
  icon,
  name,
  chipLabel,
  excerpt,
  onClick,
  isProvider,
  actions,
  ...rest
}) => {
  const classes = useStyles();
  return (
    <Card {...rest} color={color} actions={actions} className={classes.card}>
      <div className={classes.thumb}>
        <Avatar
          src={companyLogo}
          alt="Company Logo"
          className={classes.avatar}
          boxShadow
          size="large"
        />
        <Typography variant="h2" className={classes.name}>
          {name}
        </Typography>
        <div className={classes.chipContainer}>
          <Chip label={chipLabel} size="small" onClick={onClick} hasIcon />
        </div>
        <Typography variant="body2" className={classes.excerpt}>
          {excerpt}
        </Typography>
      </div>
    </Card>
  );
};

ThumbCard.propTypes = {
  icon: node,
  chipLabel: string,
  excerpt: string,
  color: string,
  OnClick: func,
  actions: any,
  isProvider: bool,
};

ThumbCard.defaultProps = {
  isProvider: false,
};

export default ThumbCard;
