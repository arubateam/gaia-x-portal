import React, { useState } from "react";
import {
  makeStyles,
  AppBar,
  Tabs,
  Tab,
  TabPanel,
  Grid,
  Label,
  Typography,
  PrimaryButton,
  Chip,
  Icon,
} from "./index";
import clsx from "clsx";

import ListItemCard from "./ListItemCard";
import AgentCard from "./AgentCard";
import { Carousel } from "react-responsive-carousel";
import a11yProps from "../helpers/a11yProps";
import ErrorBoundary from "./ErrorBoundary";
import SetCard from "./SetCard";
import { useDispatch } from "react-redux";
import { queryAction } from "../store";
import { trigger } from "../data/useEvent";

const marketingImage = "/images/gaia-x-sd-main-image-template.jpg";

const useStyles = makeStyles((theme) => ({
  gridContainer: {
    // marginBottom: theme.spacing(2),
    flexDirection: "row",
    // [theme.breakpoints.up("md")]: {
    //   flexDirection: "row",
    // },
  },
  thumb: {
    borderRadius: theme.shape.borderRadius,
    height: "100%",
    [theme.breakpoints.down("sm")]: {
      // height: theme.spacing(38),
    },
  },
  thumbImage: {
    padding: theme.spacing(3),
    borderRadius: theme.shape.borderRadius,
    height: theme.spacing(60),
    width: theme.spacing(33),
    backgroundRepeat: "no-repeat",
    backgroundSize: "contain",
    backgroundPosition: "top center",
    transition: theme.transitions.create("background-size", {
      duration: theme.transitions.duration.complex,
      easing: theme.transitions.easing.easeIn,
    }),
  },
  attribute: {
    marginBottom: theme.spacing(2),
  },
  priceContainer: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: theme.palette.neutral.light,
    borderRadius: theme.shape.borderRadius,
    padding: theme.spacing(1),
    width: theme.spacing(20),
    marginBottom: theme.spacing(12),
  },
  priceLabel: {
    color: theme.palette.primary[900],
  },
  price: {
    alignSelf: "flex-end",
  },
  amount: {
    color: theme.palette.primary[900],
    fontSize: theme.typography.size.l,
    fontWeight: theme.typography.fontWeightMedium,
    lineHeight: 1,
  },
  per: {
    color: theme.palette.primary.main,
    fontSize: theme.typography.size.m,
    fontWeight: theme.typography.fontWeightMedium,
    lineHeight: 1,
  },
  chipContainer: {
    marginLeft: `-${theme.spacing(1)}px`,
  },
  utilContainer: {
    padding: theme.spacing(3, 3, 0, 3),
  },
  setCard: {
    height: theme.spacing(50),
    justifyContent: "space-between",
  },
}));

export const Service = (item) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [value, setValue] = useState(0);
  const handleChange = (event, newValue) => {
    return setValue(newValue);
  };

  let index = 0;
  let tabs = 0;
  return (
    <ListItemCard
      initiallyExpanded={item.initiallyExpanded}
      name={item.name}
      chipLabel={item.providedBy?.name || item.name}
      href={item?.providedBy?.uri}
      logo={item?.providedBy?.logo}
      apiType={item.apiType}
    >
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          aria-label="Tabs"
        >
          <Tab label="Details" {...a11yProps(tabs++)} />
          <Tab label="Price" {...a11yProps(tabs++)} />
          {item.screenshots.length > 0 && (
            <Tab label="Screenshots" {...a11yProps(tabs++)} />
          )}
          {item.providedBy?.agents?.length > 0 && (
            <Tab label="Contact" {...a11yProps(tabs++)} />
          )}
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={index++} paddingBottom={false}>
        <Grid
          container
          spacing={3}
          className={classes.gridContainer}
          wrap="nowrap"
        >
          <Grid item>
            <div className={classes.thumb}>
              <div
                className={classes.thumbImage}
                style={{
                  backgroundImage: `url(${
                    item.marketingImage ? item.marketingImage : marketingImage
                  })`,
                }}
              />
            </div>
          </Grid>
          <Grid item>
            <div className={classes.attribute}>
              <Label>Description</Label>
              <Typography variant="body1">
                {item?.description?.split(`\n`).map((item, i) => (
                  <span key={i}>
                    {item}
                    <br />
                  </span>
                ))}
              </Typography>
            </div>
            {item.tags && (
              <div className={classes.attribute}>
                <Label>Tags</Label>
                <Grid container spacing={1} className={classes.chipContainer}>
                  {item.tags.map((tag, i) => (
                    <Grid key={i} item>
                      <Chip label={tag} size="small" />
                    </Grid>
                  ))}
                </Grid>
              </div>
            )}
            <div className={classes.attribute}>
              <Label>Last updated</Label>
              <Typography variant="body2">{item.modified}</Typography>
            </div>
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={index++} paddingBottom={false}>
        <div className={classes.priceContainer}>
          <Label className={classes.priceLabel}>Price</Label>
          <Typography variant="body2" className={classes.price}>
            <Typography variant="caption" className={classes.amount}>
              "Coming soon"
            </Typography>{" "}
            <Typography variant="caption" className={classes.per}>
              {" "}
            </Typography>
          </Typography>
        </div>
        <Grid container spacing={2} justify="flex-end">
          <Grid item>
            <PrimaryButton>Add To Cart</PrimaryButton>
          </Grid>
        </Grid>
      </TabPanel>
      {item.screenshots.length > 0 && (
        <TabPanel value={value} index={index++} paddingBottom={false}>
          <Carousel
            showArrows={true}
            infiniteLoop={false}
            autoPlay={false}
            showStatus={false}
          >
            {item.screenshots.map((image, i) => {
              return (
                <div key={i}>
                  <img
                    src={image.contentUrl}
                    alt={image.comment || item.name}
                  />
                </div>
              );
            })}
          </Carousel>
        </TabPanel>
      )}

      {item.providedBy?.agents?.length > 0 && (
        <TabPanel value={value} index={index++} paddingBottom={false}>
          <Grid container spacing={3}>
            {item.providedBy?.agents.map((agent, i) => {
              if (!agent && !agent.fullName) return null;
              return <AgentCard key={i} {...agent} />;
            })}
          </Grid>
        </TabPanel>
      )}
      {item.utilizes.length > 0 && (
        <Grid
          container
          spacing={2}
          className={clsx(classes.gridContainer, classes.utilContainer)}
        >
          <Grid item xs={12}>
            <Label>Utilizes</Label>
          </Grid>
          {item.utilizes.map((util) => {
            return (
              <Grid item xs={4} key={util.id}>
                <SetCard
                  searchMode
                  onSearch={() => {
                    trigger("RESET_FILTER");
                    dispatch(queryAction(util.name));
                  }}
                  name={util.name}
                  chipLabel={util?.providedBy?.name}
                  excerpt={util.description}
                  marketingImage={util.marketingImage}
                  hasShadow={false}
                  className={classes.setCard}
                />
              </Grid>
            );
          })}
        </Grid>
      )}
    </ListItemCard>
  );
};

const SaveService = (props) => (
  <ErrorBoundary>
    <Service {...props}>{props.children}</Service>
  </ErrorBoundary>
);

export default SaveService;
