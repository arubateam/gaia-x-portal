import React from "react";
import { ThemeProvider, CssBaseline } from "@material-ui/core";
import defaultTheme from "./defaultTheme";

export const GaiaXThemeProvider = ({ theme = null, children, name }) => {
  return (
    <ThemeProvider theme={theme || defaultTheme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};

export default GaiaXThemeProvider;
