import { createMuiTheme } from "@material-ui/core";
const DinotRegular = "/fonts/DINOT-Regular.woff";
const DinotMedium = "/fonts/DINOT-Medium.woff";
const DinotBold = "/fonts/DINOT-Bold.woff";
const GaiaXIcons = "/fonts/gaia-x-icons.woff";

const dinotRegular = {
  fontFamily: "DINOT",
  fontStyle: "normal",
  fontDisplay: "swap" as "swap",
  fontWeight: "400",
  src: `
    local('DINOT-Regular'),
    local('DINOT-Regular'),
    url(${DinotRegular}) format('woff2')
  `,
};
const dinotMedium = {
  fontFamily: "DINOT",
  fontStyle: "normal",
  fontDisplay: "swap" as "swap",
  fontWeight: "500",
  src: `
    local('DINOT-Medium'),
    local('DINOT-Medium'),
    url(${DinotMedium}) format('woff2')
  `,
};
const dinotBold = {
  fontFamily: "DINOT",
  fontStyle: "normal",
  fontDisplay: "swap" as "swap",
  fontWeight: "700",
  src: `
    local('DINOT-Bold'),
    local('DINOT-Bold'),
    url(${DinotBold}) format('woff2')
  `,
};
const gaiaXIcons = {
  fontFamily: "GaiaXIcons",
  fontStyle: "normal",
  fontDisplay: "swap" as "swap",
  fontWeight: "400",
  src: `
    url(${GaiaXIcons}) format('woff2')
  `,
};

const theme = createMuiTheme({
  overrides: {
    MuiCssBaseline: {
      "@global": {
        // "@font-face": [dinotRegular, dinotMedium, dinotBold, gaiaXIcons],
        ".gaia-x-icons": {
          fontFamily: "GaiaXIcons",
          fontWeight: "normal",
          fontStyle: "normal",
          fontSize: "16px",
          lineHeight: 1,
          letterSpacing: "normal",
          textTransform: "none",
          display: "inline-block",
          whiteSpace: "nowrap",
          wordWrap: "normal",
          direction: "ltr",
          fontFeatureSettings: "liga",
        },
        ".gaia-x-icons.dp16": {
          fontSize: "16px",
          width: "16px",
          height: "16px",
        },
        ".gaia-x-icons.dp24": {
          fontSize: "24px",
          width: "24px",
          height: "24px",
        },
        ".gaia-x-icons.dp36": {
          fontSize: "36px",
          width: "36px",
          height: "36px",
        },
        ".gaia-x-icons.dp48": {
          fontSize: "48px",
          width: "48px",
          height: "48px",
        },
        html: {
          fontSize: 14,
        },
        body: {
          fontSize: 14,
          lineHeight: "23.8px",
          backgroundColor: "transparent",
          backgroundImage:
            "linear-gradient(to right bottom,#f8fbfc,#edf5f8,#e2eff3)",
        },
        a: {
          textDecoration: "none",
        },
      },
    },
    MuiButton: {
      root: {
        fontSize: "1rem",
        textTransform: "capitalize",
      },
    },
    MuiIconButton: {
      root: {
        padding: 14,
      },
      sizeSmall: {
        padding: 3,
      },
    },
    MuiInputLabel: {
      outlined: {
        transform: "translate(14px, 14px) scale(1)",
      },
    },
    MuiInputBase: {
      input: {
        height: "auto",
      },
    },
    MuiOutlinedInput: {
      root: {
        "&$focused $notchedOutline": {
          borderWidth: 1,
        },
      },
      input: {
        padding: "12px 14px",
      },
      marginDense: {
        marginTop: 0,
        marginBottom: 0,
      },
    },
    MuiSelect: {
      selectMenu: {
        minHeight: "auto",
      },
    },
    MuiFab: {
      root: {
        position: "fixed",
        bottom: 16,
        right: 16,
        boxShadow: "0px 4px 10px rgba(33, 33, 33, 0.25)",
      },
    },
    //@ts-ignore
    MuiAutocomplete: {
      inputRoot: {
        '&[class*="MuiOutlinedInput-root"]': {
          padding: "4px 8px 4px 14px",
        },
      },
      input: {
        padding: "8.5px 4px 7.5px 4px !important",
      },
      endAdornment: {
        top: "calc(50% - 13px)",
      },
    },
  },
  typography: {
    fontFamily: "'DINOT', sans-serif",
    fontWeightMedium: 500,
    //@ts-ignore
    size: {
      xs: "0.875rem",
      s: "0.95rem",
      m: "1rem",
      l: "1.3rem",
      xl: "1.5rem",
      xxl: "1.8rem",
    },
    letterSpacing: "0.04em",
    h1: {
      fontSize: "1.5rem",
      fontWeight: 500,
      lineHeight: "2.55rem",
      color: "#003b46",
    },
    h2: {
      fontSize: "1.3rem",
      fontWeight: 500,
      lineHeight: "2.21rem",
      color: "#212121",
    },
    h3: {
      fontSize: "1.3rem",
      fontWeight: 500,
      lineHeight: "2.21rem",
      textTransform: "uppercase",
      color: "#616161",
    },
    h4: {
      fontSize: "1.2rem",
      fontWeight: 500,
      lineHeight: "2.04rem",
      color: "#212121",
    },
    h5: {
      fontSize: "1rem",
      fontWeight: 500,
      lineHeight: "1.7rem",
      color: "#212121",
    },
    h6: {
      fontSize: "1rem",
      fontWeight: 500,
      lineHeight: "1.7rem",
      color: "#212121",
    },
    body1: {
      fontSize: "1rem",
      fontWeight: 400,
      lineHeight: "1.7rem",
      color: "#212121",
    },
    body2: {
      fontSize: "0.875rem",
      fontWeight: 400,
      lineHeight: "1.49rem",
      color: "#212121",
    },
  },
  dimensions: {
    tableRowHeight: 40,
    inputHeight: 40,
    navigationWidth: 80,
    shadowCard:
      "0px 2px 1px -1px rgba(0, 25, 32, 0.1), 0px 1px 1px 0px rgba(0, 25, 32, 0.04), 0px 1px 3px 0px rgba(0, 25, 32, 0.02)",
    shadowCardHover:
      "0px 3px 1px -2px rgba(0, 25, 32,0.3),0px 2px 2px 0px rgba(0, 25, 32,0.24),0px 1px 5px 0px rgba(0, 25, 32,0.22)",
    shadowPopover:
      "0px 2px 1px -1px rgba(0,0,0, 0.2), 0px 1px 1px 0px rgba(0,0,0, 0.14), 0px 1px 3px 0px rgba(0,0,0, 0.12)",
    shadowLarge: "0px 4px 10px rgba(33, 33, 33, 0.25)",
    shadowDialog: "0 30px 20px -10px rgba(0,0,0,0.7)",
  },
  shape: {
    borderRadius: 8,
  },
  palette: {
    //@ts-ignore
    neutral: {
      50: "#f8fbfc",
      100: "#edf5f8",
      200: "#e2eff3",
      300: "#d6e9ee",
      400: "#cde4ea",
      500: "#c4dfe6",
      600: "#bedbe3",
      700: "#b6d7df",
      800: "#afd2db",
      900: "#a2cad5",
      1000: "#4D707E",
      A100: "#ffffff",
      A200: "#ffffff",
      A400: "#ffffff",
      A700: "#fcfeff",
      main: "#c4dfe6",
      light: "#d6e9ee",
      dark: "#b6d7df",
    },
    primary: {
      50: "#e0e7e9",
      100: "#b3c4c8",
      200: "#809da3",
      300: "#4D767E",
      400: "#265862",
      500: "#003b46",
      600: "#00353f",
      700: "#002d37",
      800: "#00262f",
      900: "#001920",
      A100: "#5dccff",
      A200: "#2abcff",
      A400: "#00a8f6",
      A700: "#0097dc",
      main: "#003b46",
      light: "#4D767E",
      dark: "#002d37",
    },
    secondary: {
      50: "#e0f6f9",
      100: "#b3e8f1",
      200: "#80d9e8",
      300: "#4dc9df",
      400: "#26bed8",
      500: "#00b2d1",
      600: "#00abcc",
      700: "#00a2c6",
      800: "#0099c0",
      900: "#008ab5",
      A100: "#def6ff",
      A200: "#abe8ff",
      A400: "#78daff",
      A700: "#5ed3ff",
      main: "#00b2d1",
      light: "#4dc9df",
      dark: "#00a2c6",
    },
    text: {
      primary: "#212121",
      secondary: "#616161",
      disabled: "#bdbdbd",
    },
    error: {
      light: "#ff867c",
      main: "#ef5350",
      dark: "#b61827",
    },
    warning: {
      light: "#ffd95b",
      main: "#ffa726",
      dark: "#db8400",
    },
    success: {
      light: "#98ee99",
      main: "#66bb6a",
      dark: "#338a3e",
    },
    info: {
      light: "#80d6ff",
      main: "#42a5f5",
      dark: "#0077c2",
    },
    gradients: {
      background: "linear-gradient(to right bottom,#f8fbfc,#edf5f8,#e2eff3)",
      default:
        "linear-gradient(169.84deg, rgba(224, 231, 233, 0) 3.09%, rgba(224, 231, 233, 0.414141) 16.65%, #E0E7E9 40.79%)",
      success:
        "linear-gradient(169.84deg, rgba(152, 238, 153, 0) 3.09%, rgba(152, 238, 153, 0.414141) 16.65%, #98ee99 40.79%)",
    },
  },
});

export default theme;
