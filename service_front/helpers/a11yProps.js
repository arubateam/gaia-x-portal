export default function a11yProps(index) {
  return {
    "aria-controls": `tabpanel-${index}`,
    id: `tab-${index}`,
  };
}
