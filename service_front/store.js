import { useMemo } from "react";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

let store;

const exampleInitialState = {
  query: "",
};

export const actionTypes = {
  QUERY: "QUERY",
  RESET_QUERY: "RESET_QUERY",
};

// REDUCERS
export const reducer = (state = exampleInitialState, action) => {
  switch (action.type) {
    case actionTypes.QUERY:
      return {
        ...state,
        query: action.query,
      };
    case actionTypes.RESET_QUERY:
      return {
        ...state,
        query: "",
      };
    default:
      return state;
  }
};

// ACTIONS
export const queryAction = (query) => {
  return { type: actionTypes.QUERY, query };
};
export const resetAction = () => {
  return { type: actionTypes.RESET_QUERY };
};

const persistConfig = {
  key: "primary",
  storage,
  whitelist: ["query"], // place to select which state you want to persist
};

const persistedReducer = persistReducer(persistConfig, reducer);

function makeStore(initialState = exampleInitialState) {
  return createStore(
    persistedReducer,
    initialState,
    process.env.NODE_ENV === "development"
      ? composeWithDevTools(applyMiddleware())
      : applyMiddleware()
  );
}

export const initializeStore = (preloadedState) => {
  let _store = store ?? makeStore(preloadedState);

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = makeStore({
      ...store.getState(),
      ...preloadedState,
    });
    // Reset the current store
    store = undefined;
  }

  // For SSG and SSR always create a new store
  if (typeof window === "undefined") return _store;
  // Create the store once in the client
  if (!store) store = _store;

  return _store;
};

export function useStore(initialState) {
  const store = useMemo(() => initializeStore(initialState), [initialState]);
  return store;
}
