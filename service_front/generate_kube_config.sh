#!/bin/sh

cat <<EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: service-front
  annotations:
    app.gitlab.com/app: $CI_PROJECT_PATH_SLUG
    app.gitlab.com/env: $CI_ENVIRONMENT_SLUG
spec:
  replicas: 1
  selector: 
    matchLabels:
      app: service-front
  template:
    metadata:
      labels:
        app: service-front
    spec:
      containers:
      - name: service-front
        image: ${CI_REGISTRY_IMAGE}/service_front:${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}
        imagePullPolicy: Always
        ports:
        - containerPort: 3000
---
apiVersion: v1
kind: Service
metadata:
  name: service-front
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 3000
  selector:
    app: service-front
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: ingress-front
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /\$1  # default ingress for k8s is nginx
    kubernetes.io/ingress.class: "nginx"
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
spec:
  tls:
  - hosts:
    - ${CI_ENVIRONMENT_NAME}.${DOMAIN}
    secretName: ingress-tls
  rules:
  - host: ${CI_ENVIRONMENT_NAME}.${DOMAIN}
    http:
      paths:
      - path: /?(.*)
        backend:
          serviceName: service-front
          servicePort: 80
EOF
