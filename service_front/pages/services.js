import React, { useEffect, useState } from "react";
import {
  makeStyles,
  Container,
  CircularProgress,
  SearchField,
} from "../components";
import Section from "../components/Section";
import Service from "../components/Service";
import Filter from "../components/Filter";
import Portal from "../components/Portal";
import { withApollo } from "../libs/apollo";
import { useQuery, gql } from "@apollo/react-hooks";
import { useSearch } from "../data";

import { useSelector, useDispatch } from "react-redux";
import { queryAction, resetAction } from "../store";

const generateQuery = (query = {}) => {
  const params = Object.entries(query)
    .filter(([, value]) => !!value.length)
    .map(([edge, value]) => [edge, value.map((value) => `"${value}"`)])
    .map(([edge, value]) => `${edge}: [${value.join(", ")}]`)
    .join(",");

  return gql`
    {
      services ${params ? `(${params})` : ""} {
        id
        name
        description
        tags
        apiType
        modified
        marketingImage
        screenshots {
          contentUrl
          comment
        }
        utilizes {
          id
          name
          marketingImage
          description
          providedBy{
            name
          }
        }
        providedBy {
          id
          name
          logo
          agents {
            fullName
            mail
            phone
            type
          }
        }
      }
    }
  `;
};

const useStyles = makeStyles((theme) => ({
  article: {
    display: "grid",
    gridTemplateRows: "1fr",
    gridTemplateColumns: "280px 888px",
    columnGap: theme.spacing(8),
    [theme.breakpoints.down(1400)]: {
      gridTemplateColumns: "250px 1fr",
    },
    [theme.breakpoints.between("xs", "sm")]: {
      gridTemplateColumns: "1fr",
    },
  },
  filter: {
    marginTop: theme.spacing(9),
    [theme.breakpoints.between("xs", "sm")]: {
      display: "none",
    },
  },
  searchField: {
    marginBottom: theme.spacing(4),
  },
  gridContainer: {
    flexDirection: "column",
    [theme.breakpoints.up("md")]: {
      flexDirection: "row",
    },
  },
  thumb: {
    borderRadius: theme.shape.borderRadius,
    height: "100%",
    [theme.breakpoints.down("sm")]: {
      height: theme.spacing(38),
    },
  },
  thumbImage: {
    padding: theme.spacing(3),
    borderRadius: theme.shape.borderRadius,
    height: "100%",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "right bottom",
    transition: theme.transitions.create("background-size", {
      duration: theme.transitions.duration.complex,
      easing: theme.transitions.easing.easeIn,
    }),
  },
  attribute: {
    marginBottom: theme.spacing(2),
  },
  cardHeader: {
    display: "flex",
    alignItems: "center",
  },
  flag: {
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    height: 15,
    width: 15,
    borderRadius: theme.shape.borderRadius,
    marginRight: theme.spacing(1),
    boxShadow: theme.dimensions.shadowCard,
  },
  priceContainer: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: theme.palette.neutral.light,
    borderRadius: theme.shape.borderRadius,
    padding: theme.spacing(1),
    width: theme.spacing(20),
    marginBottom: theme.spacing(12),
  },
  priceLabel: {
    color: theme.palette.primary[900],
  },
  price: {
    alignSelf: "flex-end",
  },
  amount: {
    color: theme.palette.primary[900],
    fontSize: theme.typography.size.l,
    fontWeight: theme.typography.fontWeightMedium,
    lineHeight: 1,
  },
  per: {
    color: theme.palette.primary.main,
    fontSize: theme.typography.size.m,
    fontWeight: theme.typography.fontWeightMedium,
    lineHeight: 1,
  },
  loadingContainer: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    padding: theme.spacing(4),
  },
  loading: {
    width: "40px !important",
    height: "40px !important",
  },
}));

const ServiceList = ({ services = [] }) => {
  const initiallyExpanded = services?.length === 1 ? true : false;
  return (
    <>
      {services.map((item) => (
        <Service
          key={item.id}
          id={item.id}
          {...item}
          initiallyExpanded={initiallyExpanded}
        />
      ))}
    </>
  );
};

const FetchServices = ({ filter }) => {
  const classes = useStyles();

  const query = useSearch((state) => state.query["Service"]);
  const { loading, error, data = {} } = useQuery(generateQuery(query), {
    partialRefetch: false,
    returnPartialData: true,
  });
  const services = data?.services?.filter(({ name = "", tags = [] }) => {
    return (
      name?.toLowerCase().includes(filter.toLowerCase()) ||
      tags?.find((tag) => tag.toLowerCase().includes(filter.toLowerCase()))
    );
  });
  return (
    <>
      {loading && (
        <div className={classes.loadingContainer}>
          <CircularProgress className={classes.loading} />
        </div>
      )}
      {!loading && !error && <ServiceList services={services} />}
    </>
  );
};

const ServiceListWithApollo = withApollo({ ssr: false })(FetchServices);

const Services = () => {
  const classes = useStyles();
  const query = useSelector((state) => state.query);
  const dispatch = useDispatch();

  return (
    <Portal>
      <Container maxWidth="lg" component="article" className={classes.article}>
        <div className={classes.filter}>
          <Section name="Filter" isH2>
            <Filter defaultValue="Service" />
          </Section>
        </div>
        <div className={classes.services}>
          <SearchField
            value={query}
            onChange={(e) => dispatch(queryAction(e.target.value))}
            onClose={() => dispatch(resetAction())}
            name="Search for Services"
            className={classes.searchField}
          />
          <Section name="Services" hasSortBy hasFilter>
            <ServiceListWithApollo filter={query} />
          </Section>
        </div>
      </Container>
    </Portal>
  );
};

export default Services;
