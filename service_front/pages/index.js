import React, { useState } from "react";
import {
  makeStyles,
  Container,
  SearchField,
  PrimaryButton,
  Typography,
  Link,
  Select,
  MenuItem,
  Grid,
} from "../components";

import Portal from "../components/Portal";
import AboutGAIAX from "../components/AboutGAIAX";
import { useSelector, useDispatch } from "react-redux";
import { queryAction, resetAction } from "../store";
const hero = "/images/gaia-x-hero-nodes.jpg";

const useStyles = makeStyles((theme) => ({
  article: {
    display: "grid",
    gridTemplateRows: "1fr",
    gridTemplateColumns: "960px",
    columnGap: theme.spacing(8),
    [theme.breakpoints.between("xs", "sm")]: {
      gridTemplateColumns: "1fr",
    },
  },
  hero: {
    height: theme.spacing(38),
    borderRadius: theme.shape.borderRadius,
    backgroundImage: ({ hero }) => `url(${hero})`,
    backgroundSize: "cover",
    backgroundPosition: "bottom right",
    backgroundRepeat: "no-repeat",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-end",
    boxShadow: theme.dimensions.shadowCard,
    padding: theme.spacing(0, 4),
    marginBottom: theme.spacing(6),
    "& h1": {
      fontSize: `calc(${theme.typography.size.xl} * 2)`,
      marginBottom: theme.spacing(1),
    },
    "& h2": {
      color: theme.palette.primary[300],
      fontSize: theme.typography.size.l,
      marginBottom: theme.spacing(3),
    },
  },
  button: {
    margin: theme.spacing(2, 0, 4, 0),
  },
}));

const Home = () => {
  const classes = useStyles({ hero });
  const query = useSelector((state) => state.query);
  const dispatch = useDispatch();
  const [option, setOption] = useState("services");
  const handleChange = (event) => {
    setOption(event.target.value);
  };

  return (
    <Portal>
      <Container maxWidth="md" component="article" className={classes.article}>
        <div className={classes.hero}>
          <Typography variant="h1">Welcome to GAIA-X</Typography>
          <Typography variant="h2">
            A Federated Data Infrastructure for Europe
          </Typography>
          <Grid container spacing={2}>
            <Grid item xs={2}>
              <Select value={option} onChange={handleChange}>
                <MenuItem value="services">Service</MenuItem>
                <MenuItem value="provider">Provider</MenuItem>
              </Select>
            </Grid>
            <Grid item xs={10}>
              <SearchField
                value={query}
                onChange={(e) => dispatch(queryAction(e.target.value))}
                onClose={(e) => dispatch(resetAction())}
                name="Search for Provider or Services..."
              />
            </Grid>
          </Grid>
          <Link href={`/${option}`}>
            <PrimaryButton className={classes.button} dark>
              Search
            </PrimaryButton>
          </Link>
        </div>
        <AboutGAIAX />
      </Container>
    </Portal>
  );
};

export default Home;
