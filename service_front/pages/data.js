import React, { useState } from "react";
import {
  makeStyles,
  Container,
  AppBar,
  Tabs,
  Tab,
  TabPanel,
  Grid,
  Label,
  Typography,
  PrimaryButton,
} from "../components";
import Section from "../components/Section";
import ListItemCard from "../components/ListItemCard";
import Filter from "../components/Filter";
import Portal from "../components/Portal";

import { data } from "../helpers/dummyData";

const useStyles = makeStyles((theme) => ({
  article: {
    display: "grid",
    gridTemplateRows: "1fr",
    gridTemplateColumns: "280px 888px",
    columnGap: theme.spacing(8),
    [theme.breakpoints.down(1400)]: {
      gridTemplateColumns: "250px 1fr",
    },
    [theme.breakpoints.between("xs", "sm")]: {
      gridTemplateColumns: "1fr",
    },
  },
  filter: {
    [theme.breakpoints.between("xs", "sm")]: {
      display: "none",
    },
  },
  gridContainer: {
    marginBottom: theme.spacing(2),
    flexDirection: "column",
    [theme.breakpoints.up("md")]: {
      flexDirection: "row",
    },
  },
  thumb: {
    borderRadius: theme.shape.borderRadius,
    height: "100%",
    [theme.breakpoints.down("sm")]: {
      height: theme.spacing(38),
    },
  },
  thumbImage: {
    padding: theme.spacing(3),
    borderRadius: theme.shape.borderRadius,
    height: "100%",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "right bottom",
    transition: theme.transitions.create("background-size", {
      duration: theme.transitions.duration.complex,
      easing: theme.transitions.easing.easeIn,
    }),
  },
  attribute: {
    marginBottom: theme.spacing(2),
  },
  cardHeader: {
    display: "flex",
    alignItems: "center",
  },
  flag: {
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    height: 15,
    width: 15,
    borderRadius: theme.shape.borderRadius,
    marginRight: theme.spacing(1),
    boxShadow: theme.dimensions.shadowCard,
  },
  priceContainer: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: theme.palette.neutral.light,
    borderRadius: theme.shape.borderRadius,
    padding: theme.spacing(1),
    width: theme.spacing(20),
    marginBottom: theme.spacing(12),
  },
  priceLabel: {
    color: theme.palette.primary[900],
  },
  price: {
    alignSelf: "flex-end",
  },
  amount: {
    color: theme.palette.primary[900],
    fontSize: theme.typography.size.l,
    fontWeight: theme.typography.fontWeightMedium,
    lineHeight: 1,
  },
  per: {
    color: theme.palette.primary.main,
    fontSize: theme.typography.size.m,
    fontWeight: theme.typography.fontWeightMedium,
    lineHeight: 1,
  },
}));

const Data = () => {
  const classes = useStyles();
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    return setValue(newValue);
  };

  function a11yProps(index) {
    return {
      "aria-controls": `tabpanel-${index}`,
      id: `tab-${index}`,
    };
  }

  return (
    <Portal>
      <Container maxWidth="lg" component="article" className={classes.article}>
        <div className={classes.filter}>
          <Section name="Filter" isH2>
            <Filter />
          </Section>
        </div>
        <div>
          <Section name="Data" hasSortBy hasFilter>
            {data.map((item, i) => {
              return (
                <ListItemCard
                  key={item.id}
                  name={item.name}
                  chipLabel={item.chipLabel}
                  stack={item.stack}
                  security={item.security}
                  availabilty={item.availabilty}
                  location={item.location}
                  flag={item.flag}
                  bgImage={item.bgImage}
                  marketingImage={item.marketingImage}
                  logo={item.logo}
                >
                  <AppBar position="static" color="default">
                    <Tabs
                      value={value}
                      onChange={handleChange}
                      indicatorColor="primary"
                      textColor="primary"
                      aria-label="Tabs"
                    >
                      <Tab label="Details" {...a11yProps(0)} />
                      <Tab label="Price" {...a11yProps(1)} />
                      <Tab label="Contact" {...a11yProps(2)} />
                    </Tabs>
                  </AppBar>
                  <TabPanel value={value} index={0} paddingBottom={false}>
                    <Grid
                      container
                      spacing={3}
                      className={classes.gridContainer}
                    >
                      <Grid item sm={12} md={4}>
                        <div className={classes.thumb}>
                          <div
                            className={classes.thumbImage}
                            style={{
                              backgroundImage: `url(${item.marketingImage})`,
                            }}
                          />
                        </div>
                      </Grid>
                      <Grid item sm={12} md={8}>
                        <div className={classes.attribute}>
                          <Label>Description</Label>
                          <Typography variant="body1">
                            {item.description}
                          </Typography>
                        </div>
                        <div className={classes.attribute}>
                          <Label>Features</Label>
                          <Typography variant="body2">
                            {item.features}
                          </Typography>
                        </div>
                        <div className={classes.attribute}>
                          <Label>Stack</Label>
                          <Typography variant="body2">{item.stack}</Typography>
                        </div>
                        <div className={classes.attribute}>
                          <Label>Security</Label>
                          <Typography variant="body2">
                            {item.security}
                          </Typography>
                        </div>
                        <div className={classes.attribute}>
                          <Label>Location</Label>
                          <div className={classes.cardHeader}>
                            <div
                              className={classes.flag}
                              role="img"
                              alt="Location"
                              style={{ backgroundImage: `url(${item.flag})` }}
                            />

                            <Typography variant="body2">
                              {item.location}
                            </Typography>
                          </div>
                        </div>
                        <div className={classes.attribute}>
                          <Label>Last updated</Label>
                          <Typography variant="body2">
                            {item.lastUpdated}
                          </Typography>
                        </div>
                      </Grid>
                    </Grid>
                  </TabPanel>
                  <TabPanel value={value} index={1} paddingBottom={false}>
                    <div className={classes.priceContainer}>
                      <Label className={classes.priceLabel}>Price</Label>
                      <Typography variant="body2" className={classes.price}>
                        <Typography
                          variant="caption"
                          className={classes.amount}
                        >
                          {item.price.amount}
                        </Typography>
                        <Typography variant="caption" className={classes.per}>
                          {item.price.per}
                        </Typography>
                      </Typography>
                    </div>
                    <Grid container spacing={2} justify="flex-end">
                      <Grid item>
                        <PrimaryButton>Add To Cart</PrimaryButton>
                      </Grid>
                    </Grid>
                  </TabPanel>
                  <TabPanel value={value} index={2} paddingBottom={false}>
                    <div className={classes.attribute}>
                      <Label>Contact</Label>
                      <Typography variant="body2">Contact</Typography>
                    </div>
                  </TabPanel>
                </ListItemCard>
              );
            })}
          </Section>
        </div>
      </Container>
    </Portal>
  );
};

export default Data;
