import React, { useState } from "react";
import {
  makeStyles,
  Container,
  TabPanel,
  AppBar,
  Tabs,
  Tab,
  Grid,
  Label,
  Typography,
  Icon,
  PrimaryButton,
  Link,
  CircularProgress,
  SearchField,
} from "../components";
import Section from "../components/Section";
import ListItemCard from "../components/ListItemCard";
import SetCard from "../components/SetCard";
import Filter from "../components/Filter";
import Portal from "../components/Portal";
import AgentCard from "../components/AgentCard";

import a11yProps from "../helpers/a11yProps";

import { withApollo } from "../libs/apollo";
import { useQuery, gql } from "@apollo/react-hooks";
import { useSearch } from "../data";

import { useSelector, useDispatch } from "react-redux";
import { queryAction, resetAction } from "../store";

const useStyles = makeStyles((theme) => ({
  article: {
    display: "grid",
    gridTemplateRows: "1fr",
    gridTemplateColumns: "280px 888px",
    columnGap: theme.spacing(8),
    [theme.breakpoints.down(1400)]: {
      gridTemplateColumns: "250px 1fr",
    },
    [theme.breakpoints.between("xs", "sm")]: {
      gridTemplateColumns: "1fr",
    },
  },
  filter: {
    marginTop: theme.spacing(9),
    [theme.breakpoints.between("xs", "sm")]: {
      display: "none",
    },
  },
  searchField: {
    marginBottom: theme.spacing(4),
  },
  gridContainer: {
    // marginBottom: theme.spacing(2),
    flexDirection: "row",
    // [theme.breakpoints.up("md")]: {
    //   flexDirection: "row",
    // },
  },
  thumb: {
    borderRadius: theme.shape.borderRadius,
    height: "100%",
    [theme.breakpoints.down("sm")]: {
      // height: theme.spacing(38),
    },
  },
  thumbImage: {
    padding: theme.spacing(3),
    borderRadius: theme.shape.borderRadius,
    height: theme.spacing(60),
    width: theme.spacing(33),
    backgroundRepeat: "no-repeat",
    backgroundSize: "contain",
    backgroundPosition: "top center",
    transition: theme.transitions.create("background-size", {
      duration: theme.transitions.duration.complex,
      easing: theme.transitions.easing.easeIn,
    }),
  },
  attribute: {
    marginBottom: theme.spacing(2),
  },
  flex: {
    display: "flex",
    alignItems: "center",
  },
  flag: {
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    height: theme.spacing(2),
    width: theme.spacing(2),
    borderRadius: theme.shape.borderRadius,
    marginRight: theme.spacing(1),
    boxShadow: theme.dimensions.shadowCard,
  },
  loadingContainer: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    padding: theme.spacing(4),
  },
  loading: {
    width: "40px !important",
    height: "40px !important",
  },
}));

const generateQuery = (query = {}) => {
  const params = Object.entries(query)
    .filter(([, value]) => !!value.length)
    .map(([edge, value]) => [edge, value.map((value) => `"${value}"`)])
    .map(([edge, value]) => `${edge}: [${value.join(", ")}]`)
    .join(",");
  return gql`
    {
      provider ${params ? `(${params})` : ""} {
        id
        name
        description
        logo
        link
        location
        certificates
        services {
          id
          name
          marketingImage
          description
        }
        agents {
          fullName
          mail
          phone
          type
        }
      }
    }
  `;
};

const getFlag = (location) => {
  switch (location) {
    case "France":
      return "/images/french-flag.svg";
    case "Germany":
      return "/images/german-flag.svg";
    case "Ireland":
      return "/images/irish-flag.svg";
    case "Spain":
      return "/images/spanish-flag.svg";
    case "Sweden":
      return "/images/swedish-flag.svg";
    case "Czech Republic":
      return "/images/czech-republic-flag.svg";
    case "Austria":
      return "/images/austrian-flag.svg";
    case "United Kingdom":
      return "/images/english-flag.svg";
    case "Italy":
      return "/images/italian-flag.svg";
    default:
      return null;
  }
};
const ProviderItem = (item) => {
  const [value, setValue] = useState(0);
  const handleChange = (event, newValue) => {
    return setValue(newValue);
  };
  const classes = useStyles();
  let index = 0;
  let tabs = 0;

  return (
    <ListItemCard
      key={item.id}
      name={item.name}
      chipLabel={item.name}
      href={item.link}
      logo={item.logo}
      location={item.location}
      isProvider
    >
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          aria-label="Tabs"
        >
          <Tab label="Details" {...a11yProps(tabs++)} />
          {item?.services?.length > 0 && (
            <Tab label="Services" {...a11yProps(tabs++)} />
          )}
          {item.agents.length > 0 && (
            <Tab label="Contact" {...a11yProps(tabs++)} />
          )}
        </Tabs>
      </AppBar>
      {/* Details */}
      <TabPanel value={value} index={index++} paddingBottom={false}>
        <Grid
          container
          spacing={3}
          className={classes.gridContainer}
          wrap="nowrap"
        >
          {/* <Grid item>
            <div className={classes.thumb}>
              <div
                className={classes.thumbImage}
                style={{ backgroundImage: `url(${item.logo})` }}
              />
            </div>
          </Grid> */}
          <Grid item>
            {item.description && (
              <div className={classes.attribute}>
                <Label>Description</Label>
                <Typography variant="body1">
                  {item.description.split(`\n`).map((item, i) => (
                    <span key={i}>
                      {item}
                      <br />
                    </span>
                  ))}
                </Typography>
              </div>
            )}
            {item.certificates && (
              <div className={classes.attribute}>
                <Label>Certificates</Label>
                <Typography variant="body2">{item.certificates}</Typography>
              </div>
            )}
            {item.location && (
              <div className={classes.attribute}>
                <Label>Location</Label>
                <div className={classes.flex}>
                  <div
                    className={classes.flag}
                    role="img"
                    alt="Location"
                    style={{
                      backgroundImage: `url(${getFlag(item.location)})`,
                    }}
                  />
                  <Typography variant="body2">{item.location}</Typography>
                </div>
              </div>
            )}
            <div className={classes.attribute}>
              <Label>Last updated</Label>
              <Typography variant="body2">{"2020-11-13"}</Typography>
            </div>
            <div className={classes.attribute}>
              <Label>Member since</Label>
              <Typography variant="body2">{"2020-11-11"}</Typography>
            </div>
          </Grid>
        </Grid>
      </TabPanel>
      {/* Services */}
      {item?.services?.length > 0 && (
        <TabPanel value={value} index={index++} paddingBottom={false}>
          <Grid container spacing={2} className={classes.attribute}>
            {item?.services?.map((service) => {
              return (
                <Grid key={service.id} item xs={4}>
                  <SetCard
                    name={service.name}
                    chipLabel={item.name}
                    excerpt={service.description}
                    marketingImage={service.marketingImage}
                    hasShadow={false}
                    actions={
                      <Link href="/services">
                        <PrimaryButton>Details</PrimaryButton>
                      </Link>
                    }
                  />
                </Grid>
              );
            })}
          </Grid>
        </TabPanel>
      )}
      {/* Contact */}
      {item?.agents?.length > 0 && (
        <TabPanel value={value} index={index++} paddingBottom={false}>
          <Grid container spacing={3}>
            {item?.agents?.map((agent, i) => {
              if (!agent && !agent.fullName) return null;
              return <AgentCard key={i} {...agent} />;
            })}
          </Grid>
        </TabPanel>
      )}
    </ListItemCard>
  );
};

const ProviderList = ({ filter = "" }) => {
  const classes = useStyles();

  const query = useSearch((state) => state.query["Provider"]);
  const { loading, error, data = {} } = useQuery(generateQuery(query));

  return (
    <>
      {loading && (
        <div className={classes.loadingContainer}>
          <CircularProgress className={classes.loading} />
        </div>
      )}
      {!loading &&
        !error &&
        data?.provider
          .filter(({ name = "" }) => {
            return name?.toLowerCase().includes(filter.toLowerCase());
          })
          .map((item) => {
            return <ProviderItem key={item.id} {...item} />;
          })}
    </>
  );
};

const ProviderListWithApollo = withApollo({ ssr: false })(ProviderList);

const Provider = () => {
  const classes = useStyles();

  function a11yProps(index) {
    return {
      "aria-controls": `tabpanel-${index}`,
      id: `tab-${index}`,
    };
  }
  const query = useSelector((state) => state.query);
  const dispatch = useDispatch();

  return (
    <Portal>
      <Container maxWidth="lg" component="article" className={classes.article}>
        <div className={classes.filter}>
          <Section name="Filter" isH2>
            <Filter defaultValue="Provider" />
          </Section>
        </div>
        <div>
          <SearchField
            value={query}
            onChange={(e) => dispatch(queryAction(e.target.value))}
            onClose={() => dispatch(resetAction())}
            name="Search for Provider"
            className={classes.searchField}
          />
          <Section name="Provider" hasSortBy hasFilter>
            <ProviderListWithApollo filter={query} />
          </Section>
        </div>
      </Container>
    </Portal>
  );
};

export default Provider;
