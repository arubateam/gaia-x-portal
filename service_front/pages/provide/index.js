import React from "react";
import {
  Card,
  CardParagraph,
  PrimaryButton,
  Container,
  Grid,
  makeStyles,
  Link,
} from "../../components";
import Section from "../../components/Section";
import ProvideCard from "../../components/ProvideCard";
import Portal from "../../components/Portal";

const useStyles = makeStyles((theme) => ({
  article: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  card: {
    flexDirection: "column",
  },
  flex: {
    display: "flex",
  },
}));

const Provide = () => {
  const classes = useStyles();
  return (
    <Portal>
      <Container maxWidth="md" component="article" className={classes.article}>
        <Grid container spacing={8}>
          <Grid item xs={12}>
            <Section name="Provide">
              <Card className={classes.card}>
                <CardParagraph>What do you want to provide?</CardParagraph>
                <Grid container direction="row" spacing={3}>
                  <Grid item xs>
                    <ProvideCard
                      icon="cog"
                      headline="Service"
                      description="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. "
                      actions={
                        <Grid container spacing={1} justify="flex-end">
                          <Grid item>
                            <Link href="/provide/service">
                              <PrimaryButton>Provide Service</PrimaryButton>
                            </Link>
                          </Grid>
                        </Grid>
                      }
                    />
                  </Grid>
                  <Grid item xs>
                    <ProvideCard
                      icon="database"
                      headline="Data"
                      description="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. "
                      actions={
                        <Grid container spacing={1} justify="flex-end">
                          <Grid item>
                            <Link href="/">
                              <PrimaryButton>Provide Data</PrimaryButton>
                            </Link>
                          </Grid>
                        </Grid>
                      }
                    />
                  </Grid>
                  <Grid item xs>
                    <ProvideCard
                      icon="node"
                      headline="Node"
                      description="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. "
                      actions={
                        <Grid container spacing={1} justify="flex-end">
                          <Grid item>
                            <Link href="/">
                              <PrimaryButton>Provide Node</PrimaryButton>
                            </Link>
                          </Grid>
                        </Grid>
                      }
                    />
                  </Grid>
                </Grid>
              </Card>
            </Section>
          </Grid>
        </Grid>
      </Container>
    </Portal>
  );
};

export default Provide;
