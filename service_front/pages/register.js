import React from "react";
import { makeStyles, Container } from "../components";

import RegisterForm from "../components/RegisterForm";
const register = "/images/gaia-x-register.jpg";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
    marginBottom: `-${theme.spacing(12)}px`,
    overflow: "hidden",
    background: `url(${register})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center center",
    [theme.breakpoints.up("md")]: {
      flexDirection: "column",
    },
  },
  formGroup: {
    flexDirection: "row",
  },
}));

const Register = () => {
  const classes = useStyles();
  return (
    <>
      <Container
        maxWidth={false}
        disableGutters
        component="article"
        className={classes.container}
      >
        <RegisterForm />
      </Container>
    </>
  );
};

export default Register;
