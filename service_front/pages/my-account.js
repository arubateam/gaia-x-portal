import React from "react";
import clsx from "clsx";
import {
  Container,
  Card,
  IconButton,
  List,
  ListItem,
  Avatar,
  Label,
  Attribute,
  TextFieldHidden,
  Grid,
  PrimaryButton,
  SecondaryButton,
  makeStyles,
  PhotoCameraIcon,
  ClearIcon,
  Link,
} from "../components";

import Section from "../components/Section";
import Portal from "../components/Portal";

const companyLogo = "/images/company-logo.png";
import { account } from "../helpers/dummyData";

const useStyles = makeStyles((theme) => ({
  card: {
    flexDirection: "column",
    position: "relative",
  },
  editIcon: {
    position: "absolute",
    right: theme.spacing(3),
    top: theme.spacing(3),
    zIndex: 2,
  },
  hiddenInput: {
    display: "none",
  },
  imageIcon: {
    bottom: theme.spacing(6),
    position: "absolute",
    zIndex: 2,
  },
  avatarEdit: {
    backgroundColor: theme.palette.action.focus,
  },
  avatarItem: {
    "& > div": {
      fontSize: theme.dimensions.fontSizeXXLarge,
      height: theme.spacing(12),
      position: "relative",
      width: theme.spacing(12),
    },
    justifyContent: "center",
    padding: theme.spacing(5),
  },
  list: {
    paddingTop: 0,
    paddingBottom: theme.spacing(3),
  },
  listItem: {
    width: `calc(100% + ${theme.spacing(6)}px)`,
    marginLeft: `-${theme.spacing(3)}px`,
  },
}));

const MyAccount = () => {
  const classes = useStyles();
  return (
    <Portal>
      <Container maxWidth="md" component="article">
        <Section name="My Account">
          <Card
            actions={
              <Grid container spacing={1} justify="flex-end">
                <Grid item>
                  <Link to="/">
                    <SecondaryButton>Cancel</SecondaryButton>
                  </Link>
                </Grid>
                <Grid item>
                  <Link to="/">
                    <PrimaryButton>Save</PrimaryButton>
                  </Link>
                </Grid>
              </Grid>
            }
            className={classes.card}
          >
            <IconButton size="small" className={classes.editIcon}>
              <ClearIcon color="secondary" />
            </IconButton>
            <List className={classes.list}>
              <ListItem className={clsx(classes.avatarItem, classes.listItem)}>
                <input
                  accept="image/*"
                  className={classes.hiddenInput}
                  id="icon-button-file"
                  type="file"
                />

                <label className={classes.imageIcon} htmlFor="icon-button-file">
                  <IconButton
                    size="small"
                    aria-label="upload picture"
                    component="span"
                  >
                    <PhotoCameraIcon />
                  </IconButton>
                </label>
                <Avatar
                  src={companyLogo}
                  alt="My Avatar"
                  className={classes.avatarEdit}
                ></Avatar>
              </ListItem>
              <ListItem className={classes.listItem}>
                <Label htmlFor="username">Username</Label>
                <TextFieldHidden
                  id="username"
                  onChange={() => void 0}
                  value={account.username}
                />
              </ListItem>
              <ListItem className={classes.listItem}>
                <Label htmlFor="companyName">Company Name</Label>
                <TextFieldHidden
                  id="companyName"
                  onChange={() => void 0}
                  value={account.companyName}
                />
              </ListItem>
              <ListItem className={classes.listItem}>
                <Label htmlFor="commercialRegister">Commercial Register</Label>
                <TextFieldHidden
                  id="commercialRegister"
                  onChange={() => void 0}
                  value={account.commercialRegister}
                />
              </ListItem>
              <ListItem className={classes.listItem}>
                <Label
                  htmlFor="registeredAddress"
                  style={{
                    flexBasis: "32.5%",
                  }}
                >
                  Registered Address
                </Label>
                <div style={{ display: "flex", flexDirection: "column" }}>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <Attribute htmlFor="registeredAddressStreetNumber">
                      Street and Number
                    </Attribute>
                    <TextFieldHidden
                      id="registeredAddressStreetNumber"
                      onChange={() => void 0}
                      value={account.registeredAddress.streetNumber}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <Attribute htmlFor="registeredAddressZipCity">
                      ZIP and City
                    </Attribute>
                    <TextFieldHidden
                      id="registeredAddressZipCity"
                      onChange={() => void 0}
                      value={account.registeredAddress.zipCity}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <Attribute htmlFor="registeredAddressCountry">
                      Country
                    </Attribute>
                    <TextFieldHidden
                      id="registeredAddressCountry"
                      onChange={() => void 0}
                      value={account.registeredAddress.country}
                    />
                  </div>
                </div>
              </ListItem>
              <ListItem className={classes.listItem}>
                <Label htmlFor="webAddress">Web Address</Label>
                <TextFieldHidden
                  id="webAddress"
                  onChange={() => void 0}
                  value={account.webAddress}
                />
              </ListItem>
              <ListItem className={classes.listItem}>
                <Label
                  htmlFor="individualContact"
                  style={{
                    flexBasis: "29.5%",
                  }}
                >
                  Individual Contact
                </Label>
                <div style={{ display: "flex", flexDirection: "column" }}>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <Attribute htmlFor="commercialEmailAddress">
                      Commercial Email Address
                    </Attribute>
                    <TextFieldHidden
                      id="commercialEmailAddress"
                      onChange={() => void 0}
                      value={account.individualContact.commercialEmailAddress}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <Attribute htmlFor="commercialPhoneNumber">
                      Commercial Phone Number
                    </Attribute>
                    <TextFieldHidden
                      id="commercialPhoneNumber"
                      onChange={() => void 0}
                      value={account.individualContact.commercialPhoneNumber}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <Attribute htmlFor="technicalEmailAddress">
                      Technical Email Address
                    </Attribute>
                    <TextFieldHidden
                      id="technicalEmailAddress"
                      onChange={() => void 0}
                      value={account.individualContact.technicalEmailAddress}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <Attribute htmlFor="technicalPhoneNumber">
                      Technical Phone Number
                    </Attribute>
                    <TextFieldHidden
                      id="technicalPhoneNumber"
                      onChange={() => void 0}
                      value={account.individualContact.technicalPhoneNumber}
                    />
                  </div>
                </div>
              </ListItem>
              <ListItem className={classes.listItem}>
                <Label htmlFor="vatNumber">VAT Number</Label>
                <TextFieldHidden
                  id="vatNumber"
                  onChange={() => void 0}
                  value={account.vatNumber}
                />
              </ListItem>
            </List>
          </Card>
        </Section>
      </Container>
    </Portal>
  );
};

export default MyAccount;
