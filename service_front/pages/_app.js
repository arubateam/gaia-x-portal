import React from "react";
import { useStore } from "../store";
import { Provider } from "react-redux";
import { persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";

import "../public/fonts/font.css";
import "../styles/globals.css";
import "../helpers/carousel.css";

import { GaiaXThemeProvider } from "../components";
import Head from "next/head";
import PersistGateLoading from "../components/PersistGateLoading";

function GaiaXPortal({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState);
  const persistor = persistStore(store, {}, function () {
    persistor.persist();
  });

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <React.Fragment>
      <Head>
        <title>GAIA-X Portal</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
      </Head>
      <GaiaXThemeProvider>
        <Provider store={store}>
          <PersistGate loading={<PersistGateLoading />} persistor={persistor}>
            <Component {...pageProps} />
          </PersistGate>
        </Provider>
      </GaiaXThemeProvider>
    </React.Fragment>
  );
}

export default GaiaXPortal;
