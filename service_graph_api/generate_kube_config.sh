#!/bin/sh

cat <<EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: service-graph-api
  annotations:
    app.gitlab.com/app: $CI_PROJECT_PATH_SLUG
    app.gitlab.com/env: $CI_ENVIRONMENT_SLUG
spec:
  replicas: 3
  selector: 
    matchLabels:
      app: service-graph-api
  template:
    metadata:
      labels:
        app: service-graph-api
    spec:
      containers:
      - name: service-graph-api
        image: ${CI_REGISTRY_IMAGE}/service_graph_api:${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}
        imagePullPolicy: Always
        ports:
        - containerPort: 4000
        env:
          - name: NEO4J_USERNAME
            value: neo4j
          - name: NEO4J_PASSWORD
            valueFrom:
              secretKeyRef:
                name: service-db-neo4j
                key: neo4j-password
          - name: NEO4J_ADDRESS
            value: bolt://service-db-neo4j
          - name: SERVER_URL
            value: https://${CI_ENVIRONMENT_NAME}.${DOMAIN}/api/v2/
---
apiVersion: v1
kind: Service
metadata:
  name: service-graph-api
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 4000
  selector:
    app: service-graph-api
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: ingress-api
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /\$1  # default ingress for k8s is nginx
    kubernetes.io/ingress.class: "nginx"
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
spec:
  tls:
  - hosts:
    - ${CI_ENVIRONMENT_NAME}.${DOMAIN}
    secretName: ingress-tls
  rules:
  - host: ${CI_ENVIRONMENT_NAME}.${DOMAIN}
    http:
      paths:
      - path: /graphql/?(.*)
        backend:
          serviceName: service-graph-api
          servicePort: 80
EOF
