import "./env";
import { ApolloServer } from "apollo-server";

import generateSchema from "./Schema";
import debug from "debug";
const logger = debug("SERVER");

generateSchema().then((schema) => {
  const server = new ApolloServer({
    schema,
    cors: process.env.NODE_ENV === "production" ? false : true,
    // introspection: true,
    // playground: true,
  });

  server.listen().then(({ url }) => {
    logger(`🚀  Server ready at ${url}`);
  });
});
