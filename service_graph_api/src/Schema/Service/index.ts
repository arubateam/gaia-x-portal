import { gql } from "apollo-server";

import { filterdServices, services } from "./resolvers";
import { selectors } from "../Selectors/resolvers";

export async function getTypeDef() {
  const params = await selectors(null, {
    filter: "Service",
  }).then((result) => {
    const edges = result.reduce((prev, cur) => {
      if (cur.edge) prev[cur.edge] = "String";
      return prev;
    }, {});
    return Object.entries(edges).reduce((prev, [key, value]) => {
      return `${prev}
        ${key}: [${value}]`;
    }, "");
  });

  return gql`
    type Service {
      id: Int
      name: String
      tags: [String]
      apiType: String
      modified: String
      description: String
      marketingImage: String
      screenshots: [Screenshot]
      providedBy: Provider
      utilizes: [Service]
    }

    type Screenshot {
      contentUrl: String
      comment: String
    }

    extend type Query {
      services(${params}): [Service]
    }
  `;
}

export const resolvers = {
  Query: {
    services,
  },
  Provider: {
    services: filterdServices((parent, args) => {
      if (parent) {
        args.providedBy = [parent.name];
      }
      return args;
    }),
  },
  Service: {
    utilizes: filterdServices((parent, args) => {
      if (parent) {
        args.utilizes_reverse = [parent.name];
      }
      return args;
    }),
  },
};
