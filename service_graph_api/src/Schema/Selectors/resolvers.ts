import db from "../../db";
import { Record } from "neo4j-driver";

import debug from "debug";
const logger = debug("SCHEMA:SELECTORS");

const isNotNull = (value) => value !== null;
const get = (property: string) => (value: Record) => value.get(property);
type Selector = {
  field: string;
  options: string[];
  edge?: string | null;
  property?: string;
};

export async function selectors(_parent, args): Promise<Selector[]> {
  let query = `MATCH (a) WITH a UNWIND LABELS(a) as label RETURN DISTINCT label`;
  logger("Fetching Selectors with Query" + query);

  if (args.filter) {
    query = `MATCH (b:${args.filter})-[e]->(a) 
      WITH a
      UNWIND LABELS(a) as label 
    RETURN DISTINCT label`;
  }
  const labels = (await db.getSession().run(query)).records
    .map(get("label"))
    .filter((label) => label !== "Resource") as string[];

  //Assemble more Informations for each Label
  const result = await Promise.all(
    labels.map(
      async (label): Promise<Selector[]> => {
        const relations = (
          await db
            .getSession()
            .run(`MATCH ()-[r]->(b:${label}) return DISTINCT TYPE(r) as rel`)
        ).records
          .map(get("rel"))
          .filter(isNotNull);

        // There are no Relations instead select Values from the Node
        if (relations.length === 0) {
          const names = (
            await db
              .getSession()
              .run(`match (n:${label}) RETURN DISTINCT n.name AS name`)
          ).records
            .map(get("name"))
            .filter(isNotNull) as string[];
          return [
            {
              field: label,
              options: names,
              property: "name",
            },
          ];
        }
        //Extracting Values behind the relations
        return await Promise.all(
          relations.map(
            async (rel): Promise<Selector> => {
              const names = (
                await db
                  .getSession()
                  .run(
                    `match () - [r] -> (n:${label}) WHERE Type(r) = "${rel}" RETURN DISTINCT n.name AS name`
                  )
              ).records
                .map(get("name"))
                .filter(isNotNull) as string[];

              return {
                field: label,
                options: names,
                edge: rel,
              };
            }
          )
        );
      }
    )
  );
  return result.flat();
}
