import { gql } from "apollo-server";

import { selectors } from "./resolvers";

export const typeDef = gql`
  type Selector {
    edge: String
    field: String
    options: [String]
    property: String
  }

  extend type Query {
    selectors(filter: String): [Selector]
  }
`;

export const resolvers = {
  Query: {
    selectors,
  },
};
