import { gql } from "apollo-server";
import { provider, providedBy } from "./resolver";

import { selectors } from "../Selectors/resolvers";

export async function getTypeDef() {
  const params = await selectors(null, {
    filter: "Provider",
  }).then((result) => {
    const edges = result.reduce((prev, cur) => {
      if (cur.edge) prev[cur.edge] = "String";
      return prev;
    }, {});
    return Object.entries(edges).reduce((prev, [key, value]) => {
      return `${prev}
        ${key}: [${value}]`;
    }, "");
  });

  return gql`
    type Agent {
      id: String
      firstName: String
      lastName: String
      fullName: String
      mail: String
      phone: String
      type: String
    }

    type Provider {
      id: String
      name: String
      logo: String
      description: String
      link: String
      agents: [Agent]
      location: String
      certificates: String
      services: [Service]
    }

    extend type Query {
      provider(${params}): [Provider]
    }
  `;
}

export const resolvers = {
  Query: {
    provider,
  },
  Service: {
    providedBy,
  },
};
