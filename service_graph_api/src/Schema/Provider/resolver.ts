import db from "../../db";
import neo4j from "neo4j-driver";
import faker from "faker";
import debug from "debug";

const logger = debug("SCHEMA:PROVIDER");

const isNotNull = (item) => item !== null;
type DbAgent = {
  agent: {
    identity: {
      low: number;
      high: number;
    };
    properties: {
      firstName: String;
      lastName: String;
      fullName: String;
      mbox: String;
      phone: String;
    };
  };
  _type: String;
};

type DbProvider = {
  identity: {
    low: number;
    high: number;
  };
  labels: string[];
  properties: {
    name: string;
    uri: string;
    description: string;
    hasRelevantCertification: String;
  };
  agents: DbAgent[] | null;
  location: String;
  logo: String;
};
function transformAgentToJs(node: DbAgent) {
  if (!node || !node.agent) return null;

  const firstname = faker.name.firstName();
  const lastName = faker.name.lastName();
  try {
    return {
      id: node.agent.identity.low,
      firstName: firstname,
      lastName: lastName,
      fullName: firstname + " " + lastName,
      mail: faker.internet.email(),
      phone: faker.phone.phoneNumber(),
      type: node._type,
    };
  } catch (e) {
    console.error(e);
    return null;
  }
}
const fetchAdditionalProperties = async (
  provider: DbProvider
): Promise<DbProvider> => {
  const id = provider.identity.low;
  const logo = (
    await db
      .getSession()
      .run(
        `MATCH (provider) - [:logo] -> (logo) WHERE ID(provider) = ${id} RETURN logo.uri`
      )
  ).records
    .map((record) => record.get("logo.uri"))
    .pop();
  const agents = (
    await db
      .getSession()
      .run(
        `MATCH (provider) - [r] -> (agent:Agent) WHERE ID(provider) = ${id} RETURN { agent: agent, _type: TYPE(r)} as agents`
      )
  ).records.map((record) => record.get("agents"));
  const location = (
    await db
      .getSession()
      .run(
        `MATCH (provider) - [:location] -> (location:Region) WHERE ID(provider) = ${id} RETURN location.name`
      )
  ).records
    .map((record) => record.get("location.name"))
    .pop();

  return {
    ...provider,
    agents,
    logo,
    location,
  };
};

function transformNeo4jToJs(node: DbProvider) {
  try {
    return {
      id: node.identity.low,
      name: node.properties.name,
      link: node.properties.uri,
      description: node.properties.description,
      certificates: node.properties.hasRelevantCertification,
      logo: node.logo,
      agents: (node.agents || []).map(transformAgentToJs).filter(isNotNull),
      location: node.location,
    };
  } catch (e) {
    console.error(e);
    return null;
  }
}

export async function provider(_parent, args) {
  let query = `MATCH (provider:Provider)
              WITH provider
              `;

  const limitBy = `MATCH ${Object.keys(args)
    .map((relation) => `(provider:Provider) - [:${relation}] -> (${relation})`)
    .join(", ")}
    WHERE 
    ${Object.entries(args)
      .map(
        ([relation, values]: [string, string[]]) =>
          `${relation}.name IN [${values
            .map((value) => `"${value}"`)
            .join(" , ")}]`
      )
      .join(" AND ")}
      WITH provider
    `;

  if (Object.keys(args).length > 0) {
    query = limitBy;
  }
  query += `RETURN DISTINCT provider`;

  logger("Fetching Provider with query " + query);
  const result = await Promise.all(
    (await db.getSession().run(query)).records
      .map((record) => ({
        ...record.get("provider"),
      }))
      .map(fetchAdditionalProperties)
  );

  return result.map(transformNeo4jToJs);
}

export async function providedBy(parent) {
  const result = await Promise.all(
    (
      await db.getSession().run(
        `MATCH (a:Service)-[:providedBy]->(provider:Provider) WHERE ID(a) = $id 
       RETURN DISTINCT provider`,
        {
          id: neo4j.int(parent.id),
        }
      )
    ).records
      .map((record) => record.get("provider"))
      .map(fetchAdditionalProperties)
  );
  return result.map(transformNeo4jToJs).pop();
}

/*
  MATCH (s:Provider) 
  OPTIONAL MATCH (s)-[r]-> (a:Agent)
  RETURN s, COLLECT({agent: a, _type: TYPE(r)})
 */
