#!/bin/sh

SD_TARGET_BRANCH='master'
# don't load SD every hour on master/portal
if [ ! ${CI_ENVIRONMENT_NAME} = "portal" ]; then 
  SD_TARGET_BRANCH='staging'
fi

cat <<EOF
apiVersion: batch/v1
kind: Job
metadata:
  name: init-neo4j-db
spec:
  template:
    spec:
      containers:
      - name: init-neo4j-db
        image: ${CI_REGISTRY_IMAGE}/init_neo4j_db:${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}
        env:
        - name: NEO4J_USERNAME
          value: neo4j
        - name: NEO4J_PASSWORD
          valueFrom:
            secretKeyRef:
              name: service-db-neo4j
              key: neo4j-password
        - name: NEO4J_HOST
          value: service-db-neo4j
        - name: URL
          value: https://storage.gra.cloud.ovh.net/v1/AUTH_6abcbbbfcb8d4f0fa5a7b4235070ab64/self-descriptions/${SD_TARGET_BRANCH}
      restartPolicy: Never
  # backoffLimit: 1
EOF

# don't load SD every hour on master/portal
if [ ${CI_ENVIRONMENT_NAME} = "portal" ]; then 
  exit 0
fi

echo "---"

cat <<EOF
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: update-neo4j-db
spec:
  schedule: "0 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: init-neo4j-db
            image: ${CI_REGISTRY_IMAGE}/init_neo4j_db:${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}
            env:
            - name: NEO4J_USERNAME
              value: neo4j
            - name: NEO4J_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: service-db-neo4j
                  key: neo4j-password
            - name: NEO4J_HOST
              value: service-db-neo4j
            - name: URL
              value: https://storage.gra.cloud.ovh.net/v1/AUTH_6abcbbbfcb8d4f0fa5a7b4235070ab64/self-descriptions/${SD_TARGET_BRANCH}
          restartPolicy: Never
EOF
