#!/bin/sh

set -e

URL="${URL:-https://storage.gra.cloud.ovh.net/v1/AUTH_6abcbbbfcb8d4f0fa5a7b4235070ab64/self-descriptions/master}/files.txt"

wget --continue $URL -O files.txt

ROOT_URL=`dirname $URL`
NEO4J_HOST=${NEO4J_HOST:-localhost}
NEO4J_ADDRESS=bolt://${NEO4J_HOST}:7687

cypher() {
    cmd="$1"
    echo "$cmd"
    cypher-shell -a neo4j://${NEO4J_HOST}:7687 "$cmd"
}

#cypher ':server change-password'

wget --tries=5 --waitretry=2 -O /dev/null http://${NEO4J_HOST}:7474 || sleep 20
wget --tries=5 --waitretry=2 -O /dev/null http://${NEO4J_HOST}:7474 || sleep 20
wget --tries=5 --waitretry=2 -O /dev/null http://${NEO4J_HOST}:7474 || sleep 20
wget --tries=5 --waitretry=2 -O /dev/null http://${NEO4J_HOST}:7474 || sleep 20
wget --tries=5 --waitretry=2 -O /dev/null http://${NEO4J_HOST}:7474

echo "Using db ${NEO4J_HOST} with user ${NEO4J_USERNAME}/${NEO4J_PASSWORD}"

cypher "MATCH (n) DETACH DELETE n;"
cypher "MATCH (n) RETURN count(n) AS count;"
cypher "CALL n10s.graphconfig.init();"
cypher "CREATE CONSTRAINT n10s_unique_uri ON (r:Resource) ASSERT r.uri IS UNIQUE;" || true
cypher 'CALL n10s.graphconfig.set({ handleVocabUris: "IGNORE", handleMultival: "ARRAY",   multivalPropList : ["https://de.wikipedia.org/wiki/GAIA-X#tag"]});'

for file in `cat  files.txt`; do
    sd_url=${ROOT_URL}/${file}
    ext="${file##*.}"
    case $ext in
    ttl)
        cypher "CALL n10s.rdf.import.fetch('${sd_url}', 'Turtle');"
        ;;
    jsonld)
        cypher "CALL n10s.rdf.import.fetch('${sd_url}', 'JSON-LD');"
        ;;
    *)
        echo "Unknown file type ${sd_url}";;
    esac
done

cypher "MATCH (n) RETURN count(n) AS count;"
